# Device APs

## List all devices
URL: http://127.0.0.1:5000/devices

Method: GET

Response:
```
[
    {
        "id": "123456",
        "timestamp": "Sat, 04 Sep 2021 18:31:48 GMT",
        "user_email": "user_email@gmail.com"
    },
    {
        "id": "abcd",
        "timestamp": "Sun, 12 Sep 2021 08:20:38 GMT",
        "user_email": "user_email@gmail.com"
    }
]
```

## Create a new device
URL: http://127.0.0.1:5000/devices/create

Method: POST

Request:
```
{
    "deviceId": "deviceId1",
    "user_email": "user@gmail.com",
    "deviceType": "SENSOR"
}
```
Response:
```
success or failure
```

## Get user devices
URL: http://127.0.0.1:5000/devices/get_device

Method: POST

Request:
```
{
    "user_email":"useremail@gmail.com"
}
```
Response:
```
[
    {
        "deviceType": "SENSOR",
        "eventType": "TEMPERATURE",
        "id": "123456",
        "last_heard": "Sat, 04 Sep 2021 18:31:48 GMT",
        "timestamp": "Sat, 04 Sep 2021 18:31:48 GMT",
        "value": "0"
    },
    {
        "deviceType": "RGB LED",
        "eventType": null,
        "id": "abcd",
        "last_heard": "Sun, 12 Sep 2021 08:20:38 GMT",
        "timestamp": "Sun, 12 Sep 2021 08:20:38 GMT",
        "value": null
    }
]
```

## Delete Device
URL: http://127.0.0.1:5000/devices/delete/{deviceId}

Method: DELETE

Response:
```
success or failure
```

## Update Device
URL: http://127.0.0.1:5000/devices/update

Method: PUT

Request:
```
{
    "deviceId": "abcd",
    "value": "45"
}
```

## Get device History
URL: http://127.0.0.1:5000/devices/history

Method: POST

Request:
```
{
    "deviceId": "123456",
    "eventType": "TEMPERATURE"
}
```

# User APIs

## Register a new user
URL: http://127.0.0.1:5000/users/register

Method: POST

Request: 
```
   "email": "user1@gmail.com",
   "firstname": "User",
   "lastname": "User",
   "gender": "Male",
   "picture": "Image link",
   "regType": "OAuth",
   "password": "user"
}
```

Success Response:
```
{
    "error": "No Error",
    "status": "success"
}
```

Failure Response:
```
{
    "error": "User already exist",
    "status": "failure"
}
```

## Get user
URL: http://127.0.0.1:5000/users/user1@gmail.com

Method: GET

Request: empty

Response:
```
[
    {
        "email": "user1@gmail.com",
        "firstname": "User",
        "gender": "Male",
        "id": null,
        "lastname": "User",
        "password": "user",
        "picture": "New Picture",
        "regDate": "Sun, 12 Sep 2021 18:34:25 GMT",
        "regType": "OAuth"
    }
]
```

## User Login
URL: http://127.0.0.1:5000/users/update

Method: POST

Request:
```
{
    "email":"user1@gmail.com",
    "password":"user"
}
```

Success Response:
```
[
    {
        "email": "user1@gmail.com",
        "firstname": "User",
        "gender": "Male",
        "id": null,
        "lastname": "User",
        "password": "user",
        "picture": "New Picture",
        "regDate": "Sun, 12 Sep 2021 18:34:25 GMT",
        "regType": "OAuth"
    }
]
```
Failure Response:
```
{
    "error": "User does not exist",
    "status": "failure"
}
```

## Update User
URL: http://127.0.0.1:5000/users/update

Method: PUT

Request:
```
{
    "email":"user1@gmail.com",
    "firstname": "User",
    "lastname": "User",
    "picture": "New Picture",
    "password":"user"
}
```

Success Response:
```
{
    "error": "No Error",
    "status": "success"
}
```

Failure Response:
```
{
    "error": "User does not exist",
    "status": "failure"
}
```