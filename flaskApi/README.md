NOTE: We are using python3 everywhere, you can also install these libraries using ```pip``` if you are using python2.7

## 1. Please update your database configurations in ```db.yaml``` file

## 2. Install Postman software to play with APIs


Use this link to copy all APIs in your postman
```
https://www.getpostman.com/collections/d000e5436b61452eaff5
```

## 3. Install flask
```
pip3 install flask
```

## 4. Install flask MySQL library 
```
pip3 install flask-mysqldb
```
### If you are getting below error while running the main.py
```
Error:
Traceback (most recent call last):
  File "main.py", line 2, in <module>
    from flask_mysqldb import MySQL
  File "/Users/xyz/Library/Python/2.7/lib/python/site-packages/flask_mysqldb/__init__.py", line 1, in <module>
    import MySQLdb
  File "/Users/xyz/Library/Python/2.7/lib/python/site-packages/MySQLdb/__init__.py", line 18, in <module>
    from . import _mysql
ImportError: dlopen(/Users/xyz/Library/Python/2.7/lib/python/site-packages/MySQLdb/_mysql.so, 2): Library not loaded: @rpath/libmysqlclient.21.dylib
  Referenced from: /Users/xyz/Library/Python/2.7/lib/python/site-packages/MySQLdb/_mysql.so
  Reason: image not found
```
Please create a symlink for expected library in /usr/local/lib
In my case, the library was present in ```/usr/local/mysql-8.0.23-macos10.15-x86_64/lib/```

```
sudo ln -s /usr/local/mysql-8.0.23-macos10.15-x86_64/lib/libmysqlclient.21.dylib /usr/localib/
```

