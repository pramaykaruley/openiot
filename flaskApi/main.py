from flask import Flask, jsonify, request
from flask_mysqldb import MySQL
import yaml

app = Flask(__name__)

#configure the database parameters
db = yaml.load(open('db.yaml'))
app.config["MYSQL_HOST"] = db['mysql_host']
app.config["MYSQL_USER"] = db['mysql_user']
app.config["MYSQL_PASSWORD"] = db['mysql_password']
app.config["MYSQL_DB"] = db['mysql_db']

# Get all devices (Not recommended to use by users)
@app.route("/devices")
def listDevices():
    cur = mysql.connection.cursor()
    resultValue = cur.execute("SELECT * from devices")

    if resultValue > 0:
        devices = cur.fetchall()
        # return {"devices": devices}
        result = []
        for device in devices:
            deviceData = {'id': device[0], 
                          'user_email': device[1],
                          'timestamp': device[2]}
            result.append(deviceData)
        return jsonify(result)
    else:
        return {}

# Get User specific devices 
'''
Request Body = 
{
    "user_email": "userspecificemail@gmail.com"
}
'''

@app.route("/devices/get_device", methods=['POST'])
def getDevice():
    req =  request.json
    query = "SELECT * from iot where deviceId in (SELECT deviceId from devices where user_email='"+ req['user_email']+"')"
    # return {'q': query}
    cur = mysql.connection.cursor()
    resultValue = cur.execute(query)
    if resultValue > 0:
        devices = cur.fetchall()
        result = []
        for device in devices:
            deviceData = {'id': device[0], 
                          'deviceType': device[1],
                          'eventType': device[2],
                          'value': device[3],
                          'timestamp': device[4],
                          'last_heard': device[5]}
            result.append(deviceData)
        return jsonify(result)
    else:
        return {}

@app.route("/devices/create", methods=['POST'])
def createDevice():
    req = request.json
    cur = mysql.connection.cursor()
    query = "INSERT INTO devices(deviceId, user_email) VALUES(%s, %s)"
    bindData = (req['deviceId'], req['user_email'])
    resultValue1 = cur.execute(query, bindData)

    query = "INSERT INTO iot(deviceId, deviceType) VALUES(%s, %s)"
    bindData = (req['deviceId'], req['deviceType'])
    resultValue2 = cur.execute(query, bindData)

    cur.close()
    if resultValue1 and resultValue2:
        mysql.connection.commit()
        return "success"
    else:
        return "failure"

@app.route("/devices/delete/<id>", methods=['DELETE'])
def deleteDevice(id):
    cur = mysql.connection.cursor()
    resultValue1 = cur.execute("DELETE FROM devices WHERE deviceId='"+id+"'")
    resultValue2 = cur.execute("DELETE FROM iot WHERE deviceId='"+id+"'")
    resultValue3 = cur.execute("DELETE FROM iot_history WHERE deviceId='"+id+"'")
    cur.close()
    if resultValue1 or resultValue2 or resultValue3:
        mysql.connection.commit()
        return "success"
    else:
        return "failure"

@app.route("/devices/update", methods=['PUT'])
def updateDevice():
    req = request.json
    cur = mysql.connection.cursor()
    value = req['value']
    deviceId = req['deviceId']
    query = "UPDATE iot SET value="+value+", last_heard=now() WHERE deviceId='"+ deviceId +"'"
    resultValue = cur.execute(query)

    if resultValue:
        mysql.connection.commit()
        return "success"
    else:
        return "failure"

@app.route("/devices/history", methods=['POST'])
def getHistory():
    req = request.json
    cur = mysql.connection.cursor()
    deviceId = req['deviceId']
    if req['eventType'] != "":
        eventType = req['eventType']
        query = "SELECT timestamp, value FROM iot_history WHERE deviceId='" + deviceId + "' and eventType='" + eventType + "'"
    else:
        query = "SELECT timestamp, value FROM iot_history WHERE deviceId='" + deviceId + "'"
    cur = mysql.connection.cursor()
    resultValue = cur.execute(query)
    if resultValue > 0:
        devices = cur.fetchall()
        result = []
        for device in devices:
            deviceData = {'timestamp': device[0],
                          'value': device[1]}
            result.append(deviceData)
        return jsonify(result)
    else:
        return {}

@app.route("/users/register", methods=['POST'])
def createUser():
    req = request.json
    cur = mysql.connection.cursor()
    query = "SELECT * FROM users WHERE email='"+ req['email'] +"'"
    resultValue = cur.execute(query)
    
    if resultValue:
        return {'status': 'failure',
                'error': 'User already exist'}
    else:
        query = "INSERT INTO users(email, firstname, lastname, gender, picture, regType, password) VALUES(%s, %s, %s, %s, %s, %s, %s)"
        bindData = (req['email'], req['firstname'], req['lastname'], req['gender'], req['picture'], req['regType'], req['password'])
        resultValue = cur.execute(query, bindData)
        cur.close()
        mysql.connection.commit()
        return {'status': 'success',
                'error': "No Error" }

@app.route("/users/<email>", methods=['GET'])
def getUserDetails(email):
    cur = mysql.connection.cursor()
    query = "SELECT * FROM users WHERE email='"+ email +"'"
    resultValue = cur.execute(query)
    

    if resultValue:
        users = cur.fetchall()
        result = []
        for user in users:
            userData = {'id': user[0],
                          'email': user[1], 
                          'firstname': user[2],
                          'lastname': user[3],
                          'gender': user[4],
                          'picture': user[5],
                          'regDate': user[6],
                          'password': user[7],
                          'regType': user[8]}
            result.append(userData)
            cur.close()
        return jsonify(result)

@app.route("/users/login", methods=['POST'])
def login():
    req = request.json
    cur = mysql.connection.cursor()
    email = req['email']
    password = req['password']
    query = "SELECT * FROM users WHERE email='"+ email +"' AND password='"+ password +"'"
    resultValue = cur.execute(query)
    
    if resultValue:
        users = cur.fetchall()
        result = []
        for user in users:
            userData = {'id': user[0],
                          'email': user[1], 
                          'firstname': user[2],
                          'lastname': user[3],
                          'gender': user[4],
                          'picture': user[5],
                          'regDate': user[6],
                          'password': user[7],
                          'regType': user[8]}
            result.append(userData)
            cur.close()
        return jsonify(result)
    else:
        return {"status": "failure", "error": "User does not exist"}

@app.route("/users/update",methods=['PUT'])
def updateUser():
    req = request.json
    cur = mysql.connection.cursor()
    query = "SELECT * FROM users WHERE email='"+ req['email'] +"'"
    resultValue = cur.execute(query)
    
    if resultValue == 0:
        return {'status': 'failure',
                'error': 'User does not exist'}
    else:
        query = "UPDATE users SET firstname='"+ req['firstname']+"', lastname='"+ req['lastname']+"', picture='"+req['picture']+"', password='"+req['password']+"'"
        resultValue = cur.execute(query)
        cur.close()
        mysql.connection.commit()
        return {'status': 'success',
                'error': "No Error" }

mysql = MySQL(app)

@app.route("/")
def hello_world():
    return "<p>Hello, World!</p>"

if __name__ == "__main__":
    app.run(debug=True)
