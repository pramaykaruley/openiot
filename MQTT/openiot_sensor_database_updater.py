import paho.mqtt.client as mqtt
import pymysql.cursors
import sys
import json

# User variable for Gateway ID
myGatewayID = "openIoT"

#User variable for database name
dbName = "openIoT"

# it is expected that this Database will already contain one table called users, iot, devices.  Create that table inside the Database with this command:
# create table as per the instructions provided in ../Web/readme_database.md file

# User variables for MQTT Broker connection
#mqttBroker = "localhost"
mqttBroker = "broker.hivemq.com"
mqttBrokerPort = 1883
mqttUser = None
mqttPassword = None

mysqlHost = "localhost"
mysqlUser = "root"
mysqlPassword = "openIoT_123"

# This callback function fires when the MQTT Broker conneciton is established.  At this point a connection to MySQL server will be attempted.
def on_connect(client, userdata, flags, rc):
    print("MQTT Client Connected")
    client.subscribe("gateway/"+myGatewayID+"/#")
    print("subscription topic: " + "gateway/"+myGatewayID+"/#");
    try:
        db = pymysql.connect(host=mysqlHost, user=mysqlUser, password=mysqlPassword, db=dbName, charset='utf8mb4', cursorclass=pymysql.cursors.DictCursor)
        db.close()
        print("MySQL Client Connected")
    except:
        sys.exit("Connection to MySQL failed")

# This function updates the sensor's information in the sensor index table
def sensor_update(db, payload):
    print("Updating database")
    cursor = db.cursor()
    # See if sensor already exists in sensors table, if not insert it, if so update it with latest information.
    deviceQuery = "EXISTS(SELECT * FROM iot WHERE deviceId = '%s')"%(payload['deviceId'])
    cursor.execute("SELECT "+deviceQuery)
    data = cursor.fetchone()

    if 'temperature' in payload:
        deviceType = "temperature"
    if 'motion' in payload:
        deviceType = "motion"
    if 'humidity' in payload:
        deviceType = "humidity"

    print("deviceType:=" + deviceType)

    if(data[deviceQuery] >= 1):
        updateRequest = "UPDATE iot SET value = %i, eventType = '%s', last_heard = CURRENT_TIMESTAMP WHERE deviceId = '%s'" % (payload[deviceType], deviceType , payload['deviceId'])
        cursor.execute(updateRequest)
        db.commit()
    else:
        insertRequest = "INSERT INTO iot(deviceId, eventType, value, timestamp) VALUES('%s',%i,CURRENT_TIMESTAMP)" % (payload['deviceId'], deviceType, payload[deviceType], CURRENT_TIMESTAMP)
        cursor.execute(insertRequest)
        db.commit()

# The callback for when a PUBLISH message is received from the MQTT Broker.
def on_message(client, userdata, msg):
    print("Transmission received")
    print("payload: "+ (msg.payload).decode("utf-8"))
    payload = json.loads((msg.payload).decode("utf-8"))
    print("payload json: "+ str(payload))
    if 'deviceId' in payload: #and ('temperature' in payload or 'motion' in payload):
        #if 'temperature' in payload or 'motion' in payload:
            print("Updating temp in database")
            db = pymysql.connect(host=mysqlHost, user=mysqlUser, password=mysqlPassword, db=dbName, charset='utf8mb4', cursorclass=pymysql.cursors.DictCursor)
            sensor_update(db,payload)
            db.close()

# Connect the MQTT Client
client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message
# client.username_pw_set(username=mqttUser, password=mqttPassword)
try:
    client.connect(mqttBroker, mqttBrokerPort, 60)
except:
    sys.exit("Connection to MQTT Broker failed")
# Stay connected to the MQTT Broker indefinitely
client.loop_forever()
