# Python program Sensor to cloud

**Install python dependencies**
```
python3 -m pip install mysql-connector-python
```

**Run python program**
```
python3 openiot_sensor_database_updater.py
```

## Example test command

**temperature sensor**
```
mosquitto_pub -h localhost -t "gateway/openIoT/sensor/" -m "{\"sensor_id\":\"123\",\"temperature\":10}" -d
```

**motion sensor**
```
mosquitto_pub -h localhost -t "gateway/openIoT/sensor/" -m "{\"deviceId\":\"3\",\"motion\":0}" -d
```

**Sample database update**
```
mysql> select * from iot;
+------------+------------+-------------+-------+---------------------+---------------------+
| deviceId   | deviceType | eventType   | value | timestamp           | last_heard          |
+------------+------------+-------------+-------+---------------------+---------------------+
| 1          | SWITCH     | NULL        |     1 | 2021-04-15 23:21:58 | 2021-04-17 00:12:06 |
| 2          | SENSOR     | TEMPERATURE |    30 | 2021-04-15 23:23:37 | 2021-04-17 00:44:33 |
| 3          | SENSOR     | MOTION      |     0 | 2021-04-17 00:12:31 | 2021-04-17 00:46:07 |
| abcdefabcd | SWITCH     | NULL        |  NULL | 2021-04-17 00:13:00 | 2021-04-17 00:13:00 |
+------------+------------+-------------+-------+---------------------+---------------------+
```

# Python program Cloud to device actions
- This module takes care of sending actions from WebUI to the device over using MQTT publish method
- It uses `Topic=gateway/openIoT/<deviceId>` as topic and message is sent in json format

**Message in json format**
```
	{
		"action":<"on"/"off">,
		"R":<0-255>,
		"G":<0-255>,
		"B":<0-255>
	}
```
**Run python module as below(indenpendently to test), WebUI uses it internally to send device actions**
e.g. 
```
python3 openiot_device_actions.py "{\"deviceId\":\"123\",\"deviceType\":\"RGB_LIGHT\",\"action\":\"on\",\"R\":100,\"G\":100,\"B\":100}"
```

**Test output**
```
Client mosqsub|7536-ubuntu received PUBLISH (d0, q0, r0, m0, 'gateway/openIoT/123/', ... (43 bytes))
{'action': 'on', 'R': 8, 'G': 109, 'B': 70}
Client mosqsub|7536-ubuntu received PUBLISH (d0, q0, r0, m0, 'gateway/openIoT/123/', ... (43 bytes))
{'action': 'on', 'R': 8, 'G': 109, 'B': 70}
```


