# openiot python program to send action to device
#
# Usage
# deviceId, json: {"deviceId":"123","action":"on/off","R":0-255,"G":0-255,"B":0-255}
# e.g.python3 openiot_device_actions.py "{\"deviceId\":\"123\",\"deviceType\":\"RGB\",\"action\":\"on\",\"R\":100,\"G\":100,\"B\":100}"
# e.g.python3 openiot_device_actions.py "{\"deviceId\":\"123\",\"deviceType\":\"SWITCH\",\"action\":\"on\"}"

import sys
import paho.mqtt.client as mqtt
import json

# User variable for Gateway ID
myGatewayID = "openIoT"

# User variables for MQTT Broker connection
#mqttBroker = "localhost"
mqttBroker = "broker.hivemq.com"
mqttBrokerPort = 1883
mqttUser = None
mqttPassword = None

#User variable devices
RGB_LED="RGB LED"
SWITCH="SWITCH"

def on_publish(client,userdata,result):             #create function for callback
    print("data published \n")
    pass

def on_connect(client, userdata, flags, rc):
    print("client connected \n")
    pass

client= mqtt.Client()                           #create client object
client.on_publish = on_publish                          #assign function to callback
client.on_connect = on_connect                         #assign function to callback
client.connect(mqttBroker, mqttBrokerPort)                                 #establish connection

# total arguments
n = len(sys.argv)
if n < 2:
    print("Pls pass json string for device actions!!")

print("\nArgument passed:", sys.argv[1])

action_string = sys.argv[1] #collect action json string for parsing

json_string = json.loads(action_string)
print(json_string)
print("deviceId: " + json_string["deviceId"] + ", deviceType: " + json_string["deviceType"])

#backup deviceId and deviceType for later use
deviceType = json_string["deviceType"]
deviceId = json_string["deviceId"]

#Remove deviceId and deviceType from json string
del json_string['deviceType']
del json_string['deviceId']

if deviceType == RGB_LED or deviceType == SWITCH:#Check if RGB string has valid value
    print("Device type :" + deviceType)
    if deviceType == RGB_LED and 'R' not in json_string:
        print("Device type RGB LED needs RGB values")
        exit()
    ret= client.publish("gateway/"+ myGatewayID + "/" + deviceId + "/", str(json_string))
else:
    print("Not a valid deviceType, currently not supported !!" + deviceType)
