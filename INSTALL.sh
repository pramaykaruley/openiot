
PLATFORM=$(lsb_release -a | grep Distributor | awk '{print $3}')
echo "*********************************************************"
echo " You are using $PLATFORM host"
echo "*********************************************************"

declare -a required_packages=('apache2' 'php' 'mysql-server' 'python', 'mosquitto', 'mosquitto-clients')
for pkg in "${required_packages[@]}"
do
	dpkg -l $pkg > /dev/null 2>&1
	if [ $? = 1 ]; then
		if [ $pkg = 'apache2' ]; then
			echo "************Installing APACHE2****************"
			sudo apt -y install apache2 > /dev/null 2>&1
			echo "Checking APACHE2 status"
			sudo apache2ctl configtest
		elif [ $pkg = "php" ]; then
			echo "************Installing PHP********************"
			sudo apt-get update > /dev/null 2>&1
			sudo apt -y install php libapache2-mod-php php-mcrypt php-mysql > /dev/null 2>&1
			printf "\nMove index.php in php dir.conf file to first position\n"
			read -p "Press ENTER to continue"
			sudo nano /etc/apache2/mods-enabled/dir.conf
			sudo systemctl restart apache2

		elif [ $pkg = "mysql" ]; then
			echo "************Installing MYSQL******************"
			sudo apt -y install mysql-server mysql-client > /dev/null 2>&1
		elif [ $pkg = "python" ]; then
			sudo apt install python3.8 >/dev/null 2>&1
		elif [ $pkg = "mosquitto" ]; then
			sudo apt-get install mosquitto
		elif [ $pkg = "mosquitto-clients" ]; then
			sudo apt-get install mosquitto-clients
		fi
	fi

done

declare -a python_packages=('paho-mqtt', 'mypysql', 'mysql-connector-python')
for pkg in "${python_packages[@]}"
do
	pip3 list | grep pkg > /dev/null 2>&1
	if [ $? = 1 ]; then
		if [ $pkg = 'paho-mqtt' ]; then
			echo "************ Installing PIP3 PAHO-MQTT ****************"
			sudo -H pip3 install --system paho-mqtt
		elif [ $pkg = 'mypysql' ]; then
			echo "************ Installing PIP3 MYPYSQL ******************"
			sudo -H pip3 install --system mypysql
		elif [ $pkg = 'mysql-connector-python' ]; then
			echo "**********Installing MYSQL-CONNECTOR-PYTHON************"
			sudo -H pip3 install --system mysql-connector-python
		fi
	fi
done


echo "======================================================================================"
echo "Cloning Project"
echo "======================================================================================"
#Check if git is installed

git --version

if [ $? != 0 ]; then
	sudo apt -y install git
fi

git clone https://gitlab.com/pramaykaruley/openiot.git -b develop
echo "======================================================================================"
echo "Move project to /var/www/html directory"
echo "Please access it using http://localhost/openiot/Web"
echo "======================================================================================"
sudo cp -r openiot/ /var/www/html/
mysql -u root -p < /var/www/html/openiot/Web/db.sql


echo "======================================================================================"
echo "Starting up MQTT Broker"
echo "======================================================================================"
sudo systemctl start mosquitto.service

