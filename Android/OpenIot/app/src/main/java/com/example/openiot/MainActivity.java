package com.example.openiot;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.Toast;

import com.etebarian.meowbottomnavigation.MeowBottomNavigation;

public class MainActivity extends AppCompatActivity {

    MeowBottomNavigation meowBottomNavigation;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        meowBottomNavigation = findViewById(R.id.bottom_navigation);

        meowBottomNavigation.add(new MeowBottomNavigation.Model(1, R.drawable.ic_details));
        meowBottomNavigation.add(new MeowBottomNavigation.Model(2, R.drawable.ic_rules));
        meowBottomNavigation.add(new MeowBottomNavigation.Model(3, R.drawable.ic_home));
        meowBottomNavigation.add(new MeowBottomNavigation.Model(4, R.drawable.ic_contributors));
        meowBottomNavigation.add(new MeowBottomNavigation.Model(5, R.drawable.ic_account));
        Intent intent = new Intent();
        String userName = (String) intent.getSerializableExtra("userName");

        //Get Logged in userdata
        SharedPreferences sharedPreferences = getSharedPreferences("SESSION", Context.MODE_PRIVATE);
        final Bundle bundle = new Bundle();
        bundle.putString("userName", sharedPreferences.getString("userName", ""));
        bundle.putString("userEmail", sharedPreferences.getString("userEmail", ""));
        bundle.putString("userGivenName", sharedPreferences.getString("userGivenName", ""));
        bundle.putString("userFamilyName", sharedPreferences.getString("userFamilyName", ""));
        bundle.putString("userPersonId", sharedPreferences.getString("userFamilyName", ""));

        meowBottomNavigation.setOnShowListener(new MeowBottomNavigation.ShowListener() {
            @Override
            public void onShowItem(MeowBottomNavigation.Model item) {
                Fragment fragment = null;
                switch (item.getId()){
                    case 1:
                        fragment = new DetailsFragment();
                        fragment.setArguments(bundle);
                        break;
                    case 2:
                        fragment = new RulesFragment();
                        fragment.setArguments(bundle);
                        break;
                    case 3:
                        fragment = new DashboardFragment();
                        fragment.setArguments(bundle);
                        break;
                    case 4:
                        fragment = new ContributorsFragment();
                        fragment.setArguments(bundle);
                        break;
                    case 5:
                        fragment = new AccountFragment();
                        fragment.setArguments(bundle);
                        break;
                }
                loadFragment(fragment);
            }
        });

        // Set Notification count
//        meowBottomNavigation.setCount(1, "10");

        // Set home fragment selected by default
        meowBottomNavigation.show(3, true);

        meowBottomNavigation.setOnClickMenuListener(new MeowBottomNavigation.ClickListener() {
            @Override
            public void onClickItem(MeowBottomNavigation.Model item) {
                // Display Toast
//                Toast.makeText(getApplicationContext(),
//                        "You Clicked "+ item.getId(), Toast.LENGTH_SHORT ).show();
            }
        });

        meowBottomNavigation.setOnReselectListener(new MeowBottomNavigation.ReselectListener() {
            @Override
            public void onReselectItem(MeowBottomNavigation.Model item) {
                // Display Toast
//                Toast.makeText(getApplicationContext(),
//                        "You reselected "+ item.getId(), Toast.LENGTH_SHORT ).show();
            }
        });
    }

    private void loadFragment(Fragment fragment) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.frame_layout, fragment)
                .commit();

    }
}