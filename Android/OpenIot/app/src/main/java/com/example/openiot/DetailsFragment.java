package com.example.openiot;

import android.graphics.Color;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.formatter.IValueFormatter;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.github.mikephil.charting.utils.ViewPortHandler;
import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class DetailsFragment extends Fragment {
    private LineChart lineChart;
    private TextView tv_deviceType, tv_eventType;
    private TextInputLayout til_device;
    private AutoCompleteTextView act_device;
    public ArrayList<String> yValues = new ArrayList<>();
    ArrayAdapter<String> arrayAdapter;
    final ArrayList<String> arrayList_deviceType = new ArrayList<>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    private void drawLineChart(final String [] xValues){
        ArrayList<Entry> yData = new ArrayList<>();
        Log.d("Device" , String.valueOf(yValues.size()));
        for(int i=0; i < yValues.size(); i++){
            yData.add(new Entry(i, Float.valueOf(yValues.get(i))));
        }

        final ArrayList<String> xData = new ArrayList<>();
        for(int i=0; i< xValues.length; i++){
                xData.add(xValues[i]);
        }

        LineDataSet lineDataSet = new LineDataSet(yData, "Sensor Data");
        lineDataSet.setAxisDependency(YAxis.AxisDependency.LEFT);
        lineDataSet.setColors(ColorTemplate.COLORFUL_COLORS);
        lineDataSet.setColor(Color.RED);

        LineData lineData = new LineData(lineDataSet);

        final XAxis xAxis = lineChart.getXAxis();

        ValueFormatter valueFormatter = new ValueFormatter() {
            @Override
            public String getFormattedValue(float value, Entry entry, int dataSetIndex, ViewPortHandler viewPortHandler) {
                super.getFormattedValue(value, entry, dataSetIndex, viewPortHandler);
                return xValues[(int) value];
            }
        };

        xAxis.setValueFormatter(valueFormatter);
        xAxis.setGranularity(1); // minimum axis-step (interval) is 1
        lineData.setValueTextSize(13f);

        lineData.setValueTextColor(Color.BLACK);
        lineChart.setData(lineData);
        lineChart.invalidate();
    }

    public  class MyXAxisValueFormatter implements IValueFormatter {
        private String[] mValues;
        public MyXAxisValueFormatter(String [] mValues){
            this.mValues = mValues;
        }

        @Override
        public String getFormattedValue(float value, Entry entry, int dataSetIndex, ViewPortHandler viewPortHandler) {
            return mValues[(int) value];
        }
    }

    private void loadDevicesInDropDown() {
        String DEVICE_URL =  getActivity().getString(R.string.URL) + "getDevices.php";
        StringRequest stringRequest = new StringRequest(Request.Method.GET, DEVICE_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //Process device list
                        try {
                            JSONArray jsonArray = new JSONArray(response);

                            for(int i=0; i<jsonArray.length(); i++){
                                JSONObject deviceObject = jsonArray.getJSONObject(i);
                                String value = deviceObject.getString("deviceId");
                                arrayList_deviceType.add(value);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error){
                        Log.d("Device Details:", error.toString());
                    }
                }) ;
        Volley.newRequestQueue(getActivity()).add(stringRequest);
        act_device.setAdapter(arrayAdapter);
        act_device.setThreshold(1);

    }

    private  void fetchValueForSelectedDevice(final String deviceId){
        yValues.clear();
        final String URL =  getActivity().getString(R.string.URL) + "getHistory.php";
        final String xValues[] = {"First","Second","Third","Fourth","Fifth","Sixth"};
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //Process device list
                        try {
                            JSONArray jsonArray = new JSONArray(response);

                            for(int i=0; i<jsonArray.length(); i++){
                                JSONObject deviceObject = jsonArray.getJSONObject(i);
                                String value = deviceObject.getString("value");
                                tv_deviceType.setText(deviceObject.getString("deviceType"));
                                tv_eventType.setText(deviceObject.getString("eventType"));
                                yValues.add(value);
                            }

                            drawLineChart(xValues);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error){
                        Log.d("Device Details:", error.toString());
                    }
                }) {
                        @Nullable
                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {
                            Map<String, String> data = new HashMap<>();
                            data.put("deviceId", deviceId);
                            return data;
                        }
                    };
        Volley.newRequestQueue(getActivity()).add(stringRequest);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_details, container, false);
        Log.d("Device Details", "View Created");
        lineChart = (LineChart) v.findViewById(R.id.lineChart);
        til_device = (TextInputLayout) v.findViewById(R.id.id_TIL_device);
        act_device = (AutoCompleteTextView) v.findViewById(R.id.id_tv_device);
        arrayAdapter = new ArrayAdapter<>(getContext(), R.layout.tv_dropdown, arrayList_deviceType);
        tv_deviceType = (TextView) v.findViewById(R.id.details_deviceType);
        tv_eventType = (TextView) v.findViewById(R.id.details_eventType);

        loadDevicesInDropDown();

        act_device.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(getContext(), arrayAdapter.getItem(position), Toast.LENGTH_SHORT).show();
                fetchValueForSelectedDevice(arrayAdapter.getItem(position));
            }
        });
        return v;

    }
}