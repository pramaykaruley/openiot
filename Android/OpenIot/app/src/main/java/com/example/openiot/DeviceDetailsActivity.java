package com.example.openiot;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;

import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.preference.Preference;
import androidx.preference.PreferenceCategory;
import androidx.preference.PreferenceFragmentCompat;
import androidx.preference.PreferenceScreen;
import androidx.preference.SwitchPreferenceCompat;

public class DeviceDetailsActivity extends AppCompatActivity {
    public static String deviceId, value, deviceType, lastupdatedon, otaversion;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings_activity);
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.settings, new SettingsFragment())
                .commit();
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        deviceId = getIntent().getSerializableExtra("deviceId").toString();
        deviceType = getIntent().getSerializableExtra("deviceType").toString();
        value = getIntent().getSerializableExtra("value").toString();
        lastupdatedon = getIntent().getSerializableExtra("lastupdatedon").toString();
        otaversion = getIntent().getSerializableExtra("otaversion").toString();

    }
    protected void launchHomeActivity(){
        Intent intent = new Intent(this, DashboardFragment.class);
        startActivity(intent);
    }

    public static class SettingsFragment extends PreferenceFragmentCompat {
        @Override
        public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
            final String URL = getActivity().getString(R.string.URL) + "updateDevice.php";
            setPreferencesFromResource(R.xml.root_preferences, rootKey);
            DeviceDetailsActivity deviceDetailsActivity = new DeviceDetailsActivity();

            findPreference("key_deviceId").setSummary(DeviceDetailsActivity.deviceId);
            findPreference("key_deviceType").setSummary(DeviceDetailsActivity.deviceType);
            findPreference("key_otaversion").setSummary(DeviceDetailsActivity.otaversion);
            findPreference("key_lastheard").setSummary(DeviceDetailsActivity.lastupdatedon);

            if(DeviceDetailsActivity.deviceType.equals("SENSOR")) {
                PreferenceScreen preferenceScreen = this.getPreferenceScreen();
                PreferenceCategory preferenceCategory = new PreferenceCategory(preferenceScreen.getContext());
                preferenceCategory.setTitle("Current Sensor Value");
                preferenceScreen.addPreference(preferenceCategory);

                Preference preference_1 = new Preference(preferenceScreen.getContext());
                preference_1.setTitle("Event Type");
                preference_1.setSummary("TEMPERATURE / HUMIIDTY");
                preferenceCategory.addPreference(preference_1);

                Preference preference_2 = new Preference(preferenceScreen.getContext());
                preference_2.setTitle("Value");
                preference_2.setSummary(DeviceDetailsActivity.value);
                preferenceCategory.addPreference(preference_2);
            }

            else if(DeviceDetailsActivity.deviceType.equals("SWITCH")){
                PreferenceScreen preferenceScreen = this.getPreferenceScreen();
                PreferenceCategory preferenceCategory = new PreferenceCategory(preferenceScreen.getContext());
                preferenceCategory.setTitle("Current Switch Position");
                preferenceScreen.addPreference(preferenceCategory);

                SwitchPreferenceCompat switchPreferenceCompat = new SwitchPreferenceCompat(preferenceScreen.getContext());
                switchPreferenceCompat.setTitle("Position");
                if(DeviceDetailsActivity.value.equals("0")){

                    switchPreferenceCompat.setDefaultValue(false);
                    switchPreferenceCompat.setIcon(R.drawable.ic_switch_off);
                }
                else{
                    switchPreferenceCompat.setIcon(R.drawable.ic_switch_on);
                    switchPreferenceCompat.setDefaultValue(true);
                }
                switchPreferenceCompat.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                    @Override
                    public boolean onPreferenceChange(Preference preference, final Object newValue) {
                        final String value = newValue.equals(true) ? "1":"0";
                        if(newValue.equals(false)){
                            preference.setIcon(R.drawable.ic_switch_off);
                        }
                        else{
                            preference.setIcon(R.drawable.ic_switch_on);
                        }
                        // Write into database for particular device id
                        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL,
                                new Response.Listener<String>() {
                                    @Override
                                    public void onResponse(String response) {

                                    }
                                }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Toast.makeText(getActivity(), "Something went wrong", Toast.LENGTH_SHORT).show();
                            }
                        }){
                            @Nullable
                            @Override
                            protected Map<String, String> getParams() throws AuthFailureError {
                                Map<String, String> data = new HashMap<>();
                                data.put("deviceId", DeviceDetailsActivity.deviceId);
                                data.put("value", value);
                                return data;
                            }
                        };
                        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
                        requestQueue.add(stringRequest);
                        return true;
                    }
                });
                switchPreferenceCompat.setSummaryOff("Switch is OFF");
                switchPreferenceCompat.setSummaryOn("Switch is ON");
                switchPreferenceCompat.setSwitchTextOff("OFF");
                switchPreferenceCompat.setSwitchTextOn("ON");
                preferenceCategory.addPreference(switchPreferenceCompat);
            }

            // Create Delete device button
            PreferenceScreen preferenceScreen = this.getPreferenceScreen();
            PreferenceCategory preferenceCategory = new PreferenceCategory(preferenceScreen.getContext());
            preferenceCategory.setTitle("Action");
            preferenceScreen.addPreference(preferenceCategory);

            Preference preference = new Preference(preferenceScreen.getContext());
            preference.setTitle("Delete Device");
            preference.setIcon(R.drawable.ic_delete_color);
            preferenceCategory.addPreference(preference);
            preference.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    String URL =  getActivity().getString(R.string.URL) + "deleteDevice.php";
                    Toast.makeText(getContext(), "Delete Device "+ DeviceDetailsActivity.deviceId + "???", Toast.LENGTH_SHORT).show();
                    StringRequest stringRequest = new StringRequest(Request.Method.POST, URL,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    if(response.equals("success")){
                                        Toast.makeText(getActivity(), "Deleted", Toast.LENGTH_SHORT).show();
                                        DeviceDetailsActivity deviceDetailsActivity = new DeviceDetailsActivity();
                                        deviceDetailsActivity.launchHomeActivity();
                                    }
                                    else{
                                        Toast.makeText(getActivity(), "Not deleted", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Toast.makeText(getActivity(), "Something went wrong", Toast.LENGTH_SHORT).show();
                        }
                    }){
                        @Nullable
                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {
                            Map<String, String> data = new HashMap<>();
                            data.put("deviceId", DeviceDetailsActivity.deviceId);
                            return data;
                        }
                    };
                    RequestQueue requestQueue = Volley.newRequestQueue(getContext());
                    requestQueue.add(stringRequest);

                    return false;
                }
            });

        }
    }
}