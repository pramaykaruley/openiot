package com.example.openiot;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class addDevice extends AppCompatActivity {
    TextInputLayout til_deviceType;
    AutoCompleteTextView act_deviceType;

    ArrayList<String> arrayList_deviceType;
    ArrayAdapter<String> arrayAdapter;

    TextInputEditText deviceId;
    Button btn;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_device);

        til_deviceType = (TextInputLayout) findViewById(R.id.id_TIL_deviceType);
        act_deviceType = (AutoCompleteTextView) findViewById(R.id.id_tv_deviceType);
        arrayList_deviceType = new ArrayList<>();

        arrayList_deviceType.add("SENSOR");
        arrayList_deviceType.add("SWITCH");
        arrayList_deviceType.add("RGB LED");
        arrayAdapter = new ArrayAdapter<>(getApplicationContext(), R.layout.tv_dropdown, arrayList_deviceType);
        act_deviceType.setAdapter(arrayAdapter);
        act_deviceType.setThreshold(1);


        // add onclick listener to addDevice Button
        btn = (Button) findViewById(R.id.btn_addDevice);
        deviceId = (TextInputEditText) findViewById(R.id.id_deviceId);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences sharedPreferences = getSharedPreferences("SESSION", Context.MODE_PRIVATE);
                final String userEmail = sharedPreferences.getString("userEmail","");
                final String deviceIdText = deviceId.getText().toString().trim();
                final String deviceTypeText = act_deviceType.getText().toString().trim();
//                Toast.makeText(getApplicationContext(), "Button clicked" + deviceIdText + " " + deviceTypeText, Toast.LENGTH_SHORT).show();
                // add data in database
                if (!deviceIdText.equals("")) {
                    String URL =  getApplicationContext().getString(R.string.URL) + "addDevices.php";
                    StringRequest stringRequest = new StringRequest(Request.Method.POST, URL,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    Log.d("Pramay", "Response: "+response);
                                    if(response.equals("success")){
                                        Toast.makeText(getApplicationContext(), "Device Added Successfully", Toast.LENGTH_SHORT).show();
                                    }
                                    else{
                                        Toast.makeText(getApplicationContext(), "Error Occurred", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            }, new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    Log.d("Pramay", "Response" + error.toString());
                                }
                    }){
                        @Nullable
                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {
                            Map<String, String> data = new HashMap<>();
                            data.put("deviceId", deviceIdText);
                            data.put("deviceType", deviceTypeText);
                            data.put("userEmail", userEmail);
                            return data;
                        }
                    };

                    RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
                    requestQueue.add(stringRequest);

                }
                else{
                    Toast.makeText(getApplicationContext(), "Fields can not be empty", Toast.LENGTH_SHORT).show();
                }
            }
        });


    }
}