package com.example.openiot;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.toolbox.StringRequest;

import org.w3c.dom.Text;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.MyViewHolder> {
    Context context;
    List<Device> mData;
    Dialog myDialog;
    private OnItemClickListerner mListener;
    public RecyclerViewAdapter(Context context, List<Device> mData) {
        this.context = context;
        this.mData = mData;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v;
        v = LayoutInflater.from(context).inflate(R.layout.item_device, parent, false);
        final MyViewHolder myViewHolder = new MyViewHolder(v, mListener);

        //create dialog

        myDialog = new Dialog(context);
        myDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        myViewHolder.linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Toast.makeText(context, "Clicked " + String.valueOf(myViewHolder.getAdapterPosition()) + mData.get(myViewHolder.getAdapterPosition()).getDeviceId(), Toast.LENGTH_SHORT).show();

                Intent intent = new Intent(v.getContext(), DeviceDetailsActivity.class);
                intent.putExtra("deviceId", mData.get(myViewHolder.getAdapterPosition()).getDeviceId());
                intent.putExtra("deviceType", mData.get(myViewHolder.getAdapterPosition()).getDeviceType());
                intent.putExtra("value", mData.get(myViewHolder.getAdapterPosition()).getValue());
                intent.putExtra("otaversion", "5.0.1");
                intent.putExtra("lastupdatedon", mData.get(myViewHolder.getAdapterPosition()).getLastHeard());

                v.getContext().startActivity(intent);
            }
        });
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.tv_deviceId.setText(mData.get(position).getDeviceId());
        holder.tv_deviceType.setText(mData.get(position).getDeviceType());
        holder.tv_value.setText(String.valueOf(mData.get(position).getValue()));
        holder.tv_lastheard.setText(mData.get(position).getLastHeard());
        holder.img.setImageResource(mData.get(position).getPhoto());
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public interface OnItemClickListerner{
        void onItemClick(int position);
        void onDeleteClick(int position);

    }

    public void setOnItemClickListener(OnItemClickListerner listener){
        mListener = listener;
    }
    public class MyViewHolder extends RecyclerView.ViewHolder{
        private LinearLayout linearLayout;


        private TextView tv_deviceId;
        private TextView tv_deviceType;
        private TextView tv_value;
        private TextView tv_lastheard;
        private ImageView img, deleteImage;

        public MyViewHolder(@NonNull View itemView, final OnItemClickListerner listener) {
            super(itemView);
            linearLayout = (LinearLayout) itemView.findViewById(R.id.device_item_ll);
            tv_deviceId = (TextView) itemView.findViewById(R.id.textDeviceId);
            tv_deviceType = (TextView) itemView.findViewById(R.id.idDeviceType);
            tv_value = (TextView) itemView.findViewById(R.id.idDeviceValue);
            tv_lastheard = (TextView) itemView.findViewById(R.id.idHeardDate);
            img = (ImageView) itemView.findViewById(R.id.idDeviceLogo);
            deleteImage = (ImageView) itemView.findViewById(R.id.delete_item);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });
            deleteImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(listener != null){
                        int position = getAdapterPosition();
                        if(position != RecyclerView.NO_POSITION){
                            listener.onDeleteClick(position);
                        }
                    }
                }
            });
        }
    }
}
