package com.example.openiot;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.nfc.TagLostException;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.load.model.FileLoader;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DashboardFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER

    private RecyclerView recyclerView;
    private List<Device> listDevices;
    private RecyclerViewAdapter mAdapter;
    private FloatingActionButton fab;
    public DashboardFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        listDevices = new ArrayList<>();

        loadDevices();
    }

    public void removeItem(final int position){
        final Device device = listDevices.get(position);

        // Remove device from database
        String URL =  getActivity().getString(R.string.URL) + "deleteDevice.php";
//        Toast.makeText(getContext(), "Delete Device "+ DeviceDetailsActivity.deviceId + "???", Toast.LENGTH_SHORT).show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if(response.equals("success")){
                            listDevices.remove(position);
                            mAdapter.notifyItemRemoved(position);
                        }
                        else{
                            Toast.makeText(getActivity(), "Not deleted", Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getActivity(), "Something went wrong", Toast.LENGTH_SHORT).show();
            }
        }){
            @Nullable
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> data = new HashMap<>();
                data.put("deviceId", device.getDeviceId());
                return data;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        requestQueue.add(stringRequest);
    }
    private void loadDevices() {
        final String DEVICE_URL =  getContext().getString(R.string.URL) + "getDevices.php";
        StringRequest stringRequest = new StringRequest(Request.Method.GET, DEVICE_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //Process device list
                        try {
                            JSONArray jsonArray = new JSONArray(response);

                            for(int i=0; i<jsonArray.length(); i++){
                                JSONObject deviceObject = jsonArray.getJSONObject(i);
                                String deviceId = deviceObject.getString("deviceId");
                                String deviceType = deviceObject.getString("deviceType");
                                String value = deviceObject.getString("value");
                                String lastHeard = deviceObject.getString("last_heard");
                                int img = 0;

                                if(deviceType.equals("SENSOR")){
                                    img = R.drawable.android_sensor;
                                }
                                else if(deviceType.equals("SWITCH")){
                                    img = R.drawable.android_switch_on;
                                }
                                else {
                                    img = R.drawable.android_sensor;
                                }

                                Device device = new Device(deviceId, deviceType, lastHeard, img, value);
                                listDevices.add(device);
                            }

                            mAdapter = new RecyclerViewAdapter(getContext(), listDevices);
                            recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
                            recyclerView.setAdapter(mAdapter);

                            mAdapter.setOnItemClickListener(new RecyclerViewAdapter.OnItemClickListerner() {
                                @Override
                                public void onItemClick(int position) {

                                }

                                @Override
                                public void onDeleteClick(int position) {
                                    removeItem(position);
                                }
                            });

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error){
                        Log.d("PramayK:", error.toString());
                    }
        });

        Volley.newRequestQueue(getActivity()).add(stringRequest);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v;
        v = inflater.inflate(R.layout.fragment_dashboard, container, false);
        recyclerView = (RecyclerView) v.findViewById(R.id.dashboardRecyclerView);
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));
        Drawable mDivider = ContextCompat.getDrawable(getActivity(), R.drawable.devider);
        // Create a DividerItemDecoration whose orientation is Horizontal
        DividerItemDecoration hItemDecoration = new DividerItemDecoration(getActivity(),
                DividerItemDecoration.HORIZONTAL);
        // Set the drawable on it
        hItemDecoration.setDrawable(mDivider);
        fab = (FloatingActionButton) v.findViewById(R.id.fabAddDevice);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), addDevice.class);
                startActivity(intent);
            }
        });

        String userEmail = getArguments().getString("userEmail");
        String userName = getArguments().getString("userName");

        Toast.makeText(getActivity(), userEmail + userName, Toast.LENGTH_SHORT).show();
        TextView tv_username = v.findViewById(R.id.tv_username);
        tv_username.setText(userName);

        return v;
    }


}