package com.example.openiot;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class AddRules extends AppCompatActivity {

    TextInputLayout til_inputdevice;
    AutoCompleteTextView act_inputdevice;

    TextInputLayout til_outputdevice;
    AutoCompleteTextView act_outputdevice;

    TextInputLayout til_event;
    AutoCompleteTextView act_event;

    TextInputLayout til_condition;
    AutoCompleteTextView act_condition;

    TextInputLayout til_onoff;
    AutoCompleteTextView act_onoff;

    ArrayList<String> outputDevices = new ArrayList<>();
    ArrayList<String> inputDevices = new ArrayList<>();

    ArrayList<String> arrayList_deviceType;
    ArrayAdapter<String> arrayAdapter;

    Button btn_addRule;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_rules);
        fetchInputOutputDevice();
        populateDropdown();
        btn_addRule = findViewById(R.id.btn_addRule);
        btn_addRule.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String URL = getApplicationContext().getString(R.string.URL) + "addRules.php";

                final TextInputEditText rule_title = findViewById(R.id.rule_title);
                final TextInputEditText threshold = findViewById(R.id.threshold);
                final TextInputEditText brightness = findViewById(R.id.brightness);

                final String inputDeviceId = act_inputdevice.getText().toString().trim();
                final String ruleTitle = rule_title.getText().toString().trim();
                final String outputDeviceId = act_outputdevice.getText().toString().trim();
                final String eventType = act_event.getText().toString().trim();
                final String condition = act_condition.getText().toString().trim();
                final String onoff = act_onoff.getText().toString().trim();
                final String tv_threshold = threshold.getText().toString().trim();
                final String tv_brightness = brightness.getText().toString().trim();

                StringRequest stringRequest = new StringRequest(Request.Method.POST, URL,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                Toast.makeText(getApplicationContext(), "Rule Added Successfully", Toast.LENGTH_SHORT).show();
                                btn_addRule.setEnabled(false);
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("Rules: ", "Response" + error.toString());
                    }
                }){
                    @Nullable
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        Map<String, String> data = new HashMap<>();
                        data.put("inputDevice", inputDeviceId);
                        data.put("eventType", eventType);
                        data.put("condition", condition);
                        data.put("threshold", tv_threshold);
                        data.put("outputDevice", outputDeviceId);
                        data.put("switchPosition", onoff);
                        data.put("brightness", tv_brightness);
                        data.put("title", ruleTitle);

                        return data;
                    }
                };

                RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
                requestQueue.add(stringRequest);
            }
        });
    }

    private void fetchInputOutputDevice(){
        final String DEVICE_URL =  getApplicationContext().getString(R.string.URL) + "getDevices.php";
        StringRequest stringRequest = new StringRequest(Request.Method.GET, DEVICE_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //Process device list
                        try {
                            JSONArray jsonArray = new JSONArray(response);

                            for(int i=0; i<jsonArray.length(); i++){
                                JSONObject deviceObject = jsonArray.getJSONObject(i);
                                String deviceId = deviceObject.getString("deviceId");
                                String deviceType = deviceObject.getString("deviceType");
                                if(deviceType.equals("SENSOR")){
                                    inputDevices.add(deviceId);
                                }
                                else{
                                    outputDevices.add(deviceId);
                                }
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error){
                        Log.d("PramayK:", error.toString());
                    }
                });

        Volley.newRequestQueue(getApplicationContext()).add(stringRequest);
    }

    private void populateDropdown(){
        // Populate Input device dropdown
        til_inputdevice = (TextInputLayout) findViewById(R.id.id_TIL_inputdevice);
        act_inputdevice = (AutoCompleteTextView) findViewById(R.id.id_tv_input_device);
        arrayList_deviceType = new ArrayList<>();

        arrayAdapter = new ArrayAdapter<>(getApplicationContext(), R.layout.tv_dropdown, inputDevices);
        act_inputdevice.setAdapter(arrayAdapter);
        act_inputdevice.setThreshold(1);

        //Populate Output device Dropdown
        til_outputdevice = (TextInputLayout) findViewById(R.id.id_TIL_outputdevice);
        act_outputdevice = (AutoCompleteTextView) findViewById(R.id.id_tv_outputdevice);
        arrayList_deviceType = new ArrayList<>();


        arrayAdapter = new ArrayAdapter<>(getApplicationContext(), R.layout.tv_dropdown, outputDevices);
        act_outputdevice.setAdapter(arrayAdapter);
        act_outputdevice.setThreshold(1);

        // Populate Event Dropdown
        til_event = (TextInputLayout) findViewById(R.id.id_TIL_eventtype);
        act_event = (AutoCompleteTextView) findViewById(R.id.id_tv_eventtype);
        arrayList_deviceType = new ArrayList<>();

        arrayList_deviceType.add("Temperature");
        arrayList_deviceType.add("Humidity");
        arrayAdapter = new ArrayAdapter<>(getApplicationContext(), R.layout.tv_dropdown, arrayList_deviceType);
        act_event.setAdapter(arrayAdapter);
        act_event.setThreshold(1);

        // Populate ON/OFF Dropdown
        til_onoff = (TextInputLayout) findViewById(R.id.id_TIL_onoff);
        act_onoff = (AutoCompleteTextView) findViewById(R.id.id_tv_onoff);
        arrayList_deviceType = new ArrayList<>();

        arrayList_deviceType.add("ON");
        arrayList_deviceType.add("OFF");
        arrayAdapter = new ArrayAdapter<>(getApplicationContext(), R.layout.tv_dropdown, arrayList_deviceType);
        act_onoff.setAdapter(arrayAdapter);
        act_onoff.setThreshold(1);

        // Populate Condition Dropdown
        til_condition = (TextInputLayout) findViewById(R.id.id_TIL_condition);
        act_condition = (AutoCompleteTextView) findViewById(R.id.id_tv_condition);
        arrayList_deviceType = new ArrayList<>();

        arrayList_deviceType.add("==");
        arrayList_deviceType.add(">=");
        arrayList_deviceType.add("<=");
        arrayList_deviceType.add(">");
        arrayList_deviceType.add("<");
        arrayList_deviceType.add("!=");

        arrayAdapter = new ArrayAdapter<>(getApplicationContext(), R.layout.tv_dropdown, arrayList_deviceType);
        act_condition.setAdapter(arrayAdapter);
        act_condition.setThreshold(1);

    }
}
