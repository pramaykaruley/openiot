package com.example.openiot;

public class Device {
    private String deviceId, deviceType, lastHeard, value;
    private int photo;

    public Device(){

    }

    public Device(String deviceId, String deviceType, String lastHeard, int photo, String value) {
        this.deviceId = deviceId;
        this.deviceType = deviceType;
        this.lastHeard = lastHeard;
        this.photo = photo;
        this.value = value;
    }


    public String getDeviceId() {
        return deviceId;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public String getLastHeard() {
        return lastHeard;
    }

    public int getPhoto() {
        return photo;
    }

    public String getValue() {
        return value;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    public void setLastHeard(String lastHeard) {
        this.lastHeard = lastHeard;
    }

    public void setPhoto(int photo) {
        this.photo = photo;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
