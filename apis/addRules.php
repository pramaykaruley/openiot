<?php
    require_once("db_config.php");
    
    $inputDevice = $_POST['inputDevice'];
    $eventType = $_POST['eventType'];
    $condition = $_POST['condition'];
    $threshold = $_POST['threshold'];
    $outputDevice = $_POST['outputDevice'];
    $switchPosition = $_POST['switchPosition'];
    $brightness = $_POST['brightness'];
    $title = $_POST['title'];

    // $response = Array();
    $rule = Array();
    $data = $tempArray = Array();

    if(($inp = file_get_contents('../rule_config.json')) != false){
        $tempArray = json_decode($inp, true);
    }

    $tempArray[] = array(
        'title' => $title,
        'inputDevice' => $inputDevice,
        'eventType' => $eventType,
        'condition' => $condition,
        'threshold' => $threshold,
        'outputDevice' => $outputDevice,
        'switchPosition' => $switchPosition,
        'brightness' => $brightness,
        'active' => "1"
    );

    echo json_encode($tempArray);
    $jsonRule = json_encode($tempArray, JSON_PRETTY_PRINT);
    $file = fopen('../rule_config.json','w+a');
    fwrite($file, $jsonRule);
    fclose($file);

?>
