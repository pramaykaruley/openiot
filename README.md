# OpenIoT

Learning Project

## What is this project all about?
The aim of this project is to provide an end to end IoT solution. From cloud to IoT end devices.
This would include

1. Web interface with OAuth support, where user can login from other 3rd party account.
2. Create user specific interface, session and certificates
3. MQTT broker support with open source database included
4. Standard IoT widgets such as switch, RGB bulb, Sensors, 
5. Create sample firmware codes to communicate to the OpenIoT endpoint
6. Add security TLS encryption for FW, MQTT and Web
7. Apply ML to analyze the data 
8. Add voice assistance support

## Architecture

### Web interface

### Database

### Open Auth


1. Go to https://console.developers.google.com/ 
2. Click on ```Credentials``` link from left panel
3. Create a new credentials using a link on top ```CREATE CREDENTIALS```
4. Select ```OAuth Client ID``` from the mentioned options
5. Select your application type
	5.1. For web application, select "Web Application"
	5.2. For Android Application, select "Android"

**For Web Application**

1. Fill the name of the OAuth Client
2. Add the redirect URL. This is the URL where you will pass your control after successful login, and click on ```CREATE``` button
3. It will pop up a screen with ```Client ID``` and ```Client Secret```, copy these keys 
4. Add these copied keys in ```Web/oauth2_config.php``` file 
	```
	$google_client->setClientId('<clientID>');
	$google_client->setClientSecret('<clientSecretKey>');
	$google_client->setRedirectUri('<Target Page URL>'); // I kept it as dashboard.php which is the main landing page after successful login.
	```

**For Android Applications**

1. Enter Name for your ClientID
2. For ```Package Name```, go to AndroidManifest.xml, it will contain the package name, 
```
For me, it is as below, this may change based on your project name.

<manifest xmlns:android="http://schemas.android.com/apk/res/android"
    package="com.example.openiot">
```
3. For ```SHA1 certificate fingerprint```
```
1. Go to Android Project in AndroidStudio
2. Click on gradle-> <ProjectName> -> Tasks -> android -> signingReport and let it build
3. It will create a SHA1 key 
4. Copy this key and add it in ```SHA1 certificate fingerprint``` textbox.
```
4, Click on ```CREATE``` button create the ClientID.

### ML

### Assistant support
#### Alexa
#### Google assistant
#### Siri

### Firmware
#### NodeMCU
#### NRF BLE devices
#### WiFi to Zigbee/BLE Gateways

## How to install and use?
Use INSTALL.sh file independently for installing all required packages and setting up a project
It installs below packages: (No need to clone complete project at start)
```
Command: sudo INSTALL.sh

1. 'apache2' 'php' 'mysql-server' 'python', 'mosquitto', 'mosquitto-clients'
2. Python packages as 'paho-mqtt', 'mypysql', 'mysql-connector-python'
3. It checks whether git is installed in your system
4. Clones the Project and places it at /var/www/html location
5. Sets up the database required for this project
	-> Database file is included in Web directory (db.sql)
	-> To setup database independently, you can use below command
	
	mysql -u root -p < /var/www/html/openiot/Web/db.sql
```

### Pre-requisits
1. Install Apache server and php
https://www.digitalocean.com/community/tutorials/how-to-install-linux-apache-mysql-php-lamp-stack-on-ubuntu-16-04

2. Install and setup MQTT broker
sudo apt-get install mosquitto
sudo apt-get install mosquitto-clients
sudo systemctl start mosquitto.service
 
3. Install python modules
sudo -H pip3 install --system paho-mqtt
sudo -H pip3 install --system mypysql
sudo -H pip3 install --system mysql-connector-python

Refer to [Project WikiPage](https://gitlab.com/pramaykaruley/openiot/-/wikis/OpenIoT-Documentation) for more details about setup process, Software info, Firmware setup info

## Screenshots
### Dashboard page
![alt text for screen readers](/images/Dashboard.png "Dashboard Page")
### Device details and history page
![alt text for screen readers](/images/Device_details_and_history.png "Device Details and History Page")
### Automation Rules
![alt text for screen readers](/images/Automation_rules.png "Automation Rules Page")

## Running Android Application
1. Open Android/OpenIot folder in AndroidStudio (It may require to change permission)
2. Setup the OpenAuth as mentioned above
3. Change the IP address in ```strings.xml``` file, (Your Host Machine's IP address where database is setup)
4. Build the project to create a apk file

### Screenshots of Android App:
![alt text for screen readers](images/combineAndroidAppImages.png "Android App")
## Contributors
Pramay Karule <pramaykaruley@gmail.com>
Shrenik Shikhare <engineershrenik@gmail.com>


## TODO

