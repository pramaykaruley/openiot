/*
 OpenIoT dummy sensor device example with Arduino IDE.
 This is based on pubsub client example in Arduino

 Prerequisits for this example
 1. Install openIoT on your linux machine(Raspberry Pi)
 2. Add one example device in openIoT from dashboard->add device option.
 3. e.g. in this example deviceId="humid1" and humidity=xx is random number
 which is sent to the desired mqtt broker, here we are using broker.hivemq.com.
 4. Remember openIoT uses json format for MQTT message. So any other sensors
 also must use right json format to send message.
 5. openIoT will collect the data from this sensor and show it on dashboard
 as well as in history
 6. Install PubSubClient, ArduinoJson library and esp8266 board libs for
 Arduino

 If any issue, please contact us

*/

#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <ArduinoJson.h>

#define PUB_TOPIC "gateway/openIoT/"
// Update these with values suitable for your network.

const char* ssid = "openIoT"; //Change ssid as per your network
const char* password = "openIoT_123"; //Change password as per your network
const char* mqtt_server = "broker.hivemq.com"; //Change as per your need

WiFiClient espClient;
PubSubClient client(espClient);
long lastMsg = 0;
char msg[50];
int value = 0;

void setup() {
  pinMode(BUILTIN_LED, OUTPUT);     // Initialize the BUILTIN_LED pin as an output
  Serial.begin(115200);
  setup_wifi();
  client.setServer(mqtt_server, 1883);
}

void setup_wifi() {

  delay(10);
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

void reconnect() {

  StaticJsonBuffer<300> JSONbuffer;
  JsonObject& JSONencoder = JSONbuffer.createObject();

  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    if (client.connect("ESP8266_openIoT_client")) {
      Serial.println("connected");

      //String stringOne =  String(random(100), DEC);
      JSONencoder["deviceId"] = "humid1";
      JSONencoder["humidity"] = random(100);//stringOne;
      //JsonArray& values = JSONencoder.createNestedArray("values");
      //values.add(20);
      //values.add(21);
      //values.add(23);

      char JSONmessageBuffer[100];
      JSONencoder.printTo(JSONmessageBuffer, sizeof(JSONmessageBuffer));
      Serial.println("Sending message to MQTT topic..");
      Serial.println(JSONmessageBuffer);

      // Once connected, publish an announcement...
      //{\"deviceId\":\"humid1\",\"humidity\":11}
      client.publish(PUB_TOPIC, JSONmessageBuffer);
      delay(5000);
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}
void loop() {

  if (!client.connected()) {
    reconnect();
  }
  client.loop();
}
