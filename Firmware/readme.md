# Firmware demo codes

## Arduino IDE based examples

### ESP8266 examples
#### mqtt based dummy sensor
source file: `openiot_dummy_sensor_esp8266.ino`

example output:

`WiFi connected`
`IP address:`
`192.168.1.11`
`Attempting MQTT connection...connected`
`Sending message to MQTT topic..`
`{"deviceId":"humid1","humidity":28}`
`Attempting MQTT connection...connected`
`Sending message to MQTT topic..`
`{"deviceId":"humid1","humidity":77}`
`Attempting MQTT connection...connected`
`Sending message to MQTT topic..`

#### mqtt based rgb light
source file: `openiot_rgb_light_esp8266.ino`

- This example needs to have RGB light with deviceId added from WebUI
- Device will get device actions in json format over mqtt
	e.g.`{"action":"on/off","R":0-255,"G":0-255,"B":0-255}` 

example output:
`Attempting MQTT connection...connected
Message arrived [gateway/openIoT/RGB_1/] {'action': 'on', 'R': 204, 'G': 25, 'B': 25}
RED:204
GREEN:25
BLUE:25
Writing values ON`

## Native SDK examples

### ESP8266 RTOS SDK example
#### mqtt based dummy sensor
Dependencies:
For this example make sure to install xtensa toolchain and ESP8266 RTOS SDK

Refer https://docs.espressif.com/projects/esp8266-rtos-sdk/en/latest/get-started/

Make sure SDK and IDF path's are set correctly, in order to compile this example

To configure wifi, broker url and open iot mqtt topic do following
1. `Example configuration->Broker url` <- edit this if needed
2. `Example configuration->openIoT MQTT Topic` <- edit this if needed
3. `Example Connection configuration->SSID` <- edit this if needed
4. `Example Connection configuration->password` <- edit this if needed
5. Set serial port as needed (e.g. `/dev/ttyUSB0`)
6. `make flash` to flash the example

source folder: `openiot_esp8266_sdk_dummy_example`

