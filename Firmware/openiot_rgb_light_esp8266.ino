/*
 OpenIoT dummy sensor device example.
 This is based on pubsub client example in Arduino

 Prerequisits for this example
 1. Install openIoT on your linux machine(Raspberry Pi)
 2. Add one example device in openIoT from dashboard->add device option.
 3. e.g. in this example deviceId="RGB_1" and action will be sent in json
 format to the deviceId from the desired mqtt broker, here we are using broker.hivemq.com.
 4. Remember openIoT uses json format for MQTT message. So any other devices
 also must use right json format to send message.
 5. Json message for RGB LED will be
 {"action":"on/off","R":0-255,"G":0-255,"B":0-255}

 If any issue, please contact us

*/

#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <ArduinoJson.h>

/*PIN configuration*/
#define CONFIG_RED_PIN 0 //use LED_BUILTIN to test one LED pwm change
#define CONFIG_GREEN_PIN 2
#define CONFIG_BLUE_PIN 3

#define DEVICE_ID "RGB_1" //TODO: Can be made configuration from wifi manager
#define SUB_TOPIC "gateway/openIoT/" DEVICE_ID "/"
#define DEVICE_STATE_ON "on"
#define DEVICE_STATE_OFF "off"

// Update these with values suitable for your network.

const char* ssid = "YourSSID";
const char* password = "YourPassword";
const char* mqtt_server = "broker.hivemq.com";

WiFiClient espClient;
PubSubClient client(espClient);

const int BUFFER_SIZE = JSON_OBJECT_SIZE(20);

long lastMsg = 0;
char msg[50];
int value = 0;
int stateOn = 0;

/*RGB values*/
int red = 0;
int blue = 0;
int green = 0;

bool processJson(char* message) {
  StaticJsonBuffer<BUFFER_SIZE> jsonBuffer;

  JsonObject& root = jsonBuffer.parseObject(message);

  if (!root.success()) {
    Serial.println("parseObject() failed");
    return false;
  }

  if (root.containsKey("action")) {
    if (strcmp(root["action"], DEVICE_STATE_ON) == 0) {
      stateOn = true;
    }
    else if (strcmp(root["action"], DEVICE_STATE_OFF) == 0) {
      stateOn = false;
    }
  }  

    if (root.containsKey("R")) {
      red = root["R"];
    }
     if (root.containsKey("G")) {
      green = root["G"];
    }
     if (root.containsKey("B")) {
      blue = root["B"];
    }
  Serial.print("RED:");
  Serial.println(red);
  Serial.print("GREEN:");
  Serial.println(green);
  Serial.print("BLUE:");
  Serial.println(blue);
  return true;
}

void write_values_to_pins(int enable){
  if(enable)
  {
    analogWrite(CONFIG_RED_PIN,red);
    analogWrite(CONFIG_GREEN_PIN,green);
    analogWrite(CONFIG_BLUE_PIN,blue);
  }
  else
  {
    analogWrite(CONFIG_RED_PIN,0);
    analogWrite(CONFIG_GREEN_PIN,0);
    analogWrite(CONFIG_BLUE_PIN,0);
  }
}

void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");

  char message[length + 1];
  for (int i = 0; i < length; i++) {
    message[i] = (char)payload[i];
  }
  message[length] = '\0';
  Serial.println(message);

  if (!processJson(message)) {
    return;
  }

  if (stateOn) {
    Serial.println("Writing values ON");
    write_values_to_pins(1);
  }
  else {
    Serial.println("Writing values OFF");
    write_values_to_pins(0);
  }
  
  send_device_status();
}

void setup() {
  
  pinMode(CONFIG_RED_PIN, OUTPUT);
  pinMode(CONFIG_GREEN_PIN, OUTPUT);
  pinMode(CONFIG_BLUE_PIN, OUTPUT);
  
  analogWriteRange(255);
  
  analogWrite(CONFIG_RED_PIN,0);
  analogWrite(CONFIG_GREEN_PIN,0);
  analogWrite(CONFIG_BLUE_PIN,0);
  
  Serial.begin(115200);
  setup_wifi();
  client.setServer(mqtt_server, 1883);
  client.setCallback(callback);
}

void setup_wifi() {

  delay(10);
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

/*Send device status to Web*/
void send_device_status(void){
  
}

void reconnect() {

  StaticJsonBuffer<300> JSONbuffer;
  JsonObject& JSONencoder = JSONbuffer.createObject();


  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    if (client.connect("ESP8266Client")) {
      Serial.println("connected");
      client.subscribe(SUB_TOPIC);
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}
void loop() {

  if (!client.connected()) {
    reconnect();
  }
  client.loop();
}
