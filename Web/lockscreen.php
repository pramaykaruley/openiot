<?php
session_start();
$_SESSION['2last_url'] = isset($_SESSION['last_url']) ? $_SESSION['last_url'] : null;
$_SESSION['last_url'] = $_SERVER['HTTP_REFERER'];

unset($_SESSION['active']);
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>AdminLTE 3 | Lockscreen</title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/adminlte.min.css">
</head>
<body class="hold-transition lockscreen">
<!-- Automatic element centering -->
<div class="lockscreen-wrapper">
  <div class="lockscreen-logo">
    <a href="index.php"><b>Open</b>IoT</a>
  </div>
  <!-- User name -->
  <div class="lockscreen-name"><?php echo $_SESSION['name']; ?></div>

  <!-- START LOCK SCREEN ITEM -->
  <div class="lockscreen-item">
    <!-- lockscreen image -->
    <div class="lockscreen-image">
      <img src="<?php echo $_SESSION['picture']; ?>" alt="User Image">
    </div>
    <!-- /.lockscreen-image -->

    <!-- lockscreen credentials (contains the form) -->
    <form class="lockscreen-credentials">
      <div class="input-group">
        <input type="password" class="form-control" placeholder="password" id="password">

        <div class="input-group-append">
          <button type="button" class="btn" onclick="unlockUser('<?php echo  $_SESSION['2last_url'];?>') ">
            <i class="fas fa-arrow-right text-muted"></i>
          </button>
        </div>
      </div>
      
    </form>
   
    <!-- /.lockscreen credentials -->

  </div>
  
  <!-- /.lockscreen-item -->
  <div class="help-block text-center">
  <div id="error"></div>
    Enter your password to retrieve your session
  </div>
  <div class="text-center">
    <a href="index.php">Or sign in as a different user</a>
  </div>

</div>

<script src="plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>

<script>
function unlockUser(last_page){
    console.log(last_page);
    var password = document.getElementById('password').value;
    if(password == ""){
        return;
    }
    $.ajax({
          url: 'userConfig.php',
          type: 'POST',
          data: ({
              lockscreen: 1,
              password: password
          }),
          cache: false,
          success:function(response){
              var data = JSON.parse(response);
              if(data.status=="true"){
                location.replace(last_page);
              }
              else{
                  document.getElementById("error").innerHTML="Incorrect Password";
              }
          }
        });
}

</script>
</body>
</html>
