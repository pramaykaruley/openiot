<?php
include "db_config.php";
session_start();

if(isset($_POST['addDevice'])){
    $deviceId = $_POST['deviceId'];
    $deviceType = $_POST['deviceType'];

    $sql1 = "INSERT INTO devices (deviceId, user_email) VALUES
    (
      '".$deviceId."', 
      '".$_SESSION['email']."'
    );";
  
    $sql2 = "INSERT INTO iot (deviceId, deviceType) VALUES
    (
      '". $deviceId ."',
      '". $deviceType ."'
    );";
  
    $result = mysqli_query($conn, $sql1);
    if($result){
        $result2 = mysqli_query($conn, $sql2);
        if($result2){
            $myobj = new \stdClass();
            $myobj->status = "true";
            $myobj->msg = "Device Added Successfully";
            echo json_encode($myobj);
            return;
        }
    }
    
    $myobj = new \stdClass();
    $myobj->status = "false";
    $myobj->msg = "Something went wrong..";
    echo json_encode($myobj);
    return;
}

if(isset($_POST['delete'])){
    $deviceId = $_POST['deviceId'];
    $sql1 = "DELETE FROM iot WHERE deviceId='".$deviceId."';";
    $sql2 = "DELETE FROM devices WHERE deviceId='".$deviceId."';";
    $sql3 = "DELETE FROM iot_history WHERE deviceId='".$deviceId."';";
    mysqli_query($conn, $sql1);
    mysqli_query($conn, $sql2);
    mysqli_query($conn, $sql3);
}

if(isset($_POST['update'])){
    $deviceType = $_POST['deviceType'];
    $deviceId = substr($_POST['deviceId'], strpos($_POST['deviceId'], "_") + 1);
    $value = ltrim($str, $_POST['value'][0]);
    $sql1 = "UPDATE iot SET value='".$_POST['value']."', last_heard=now() WHERE deviceId='".$deviceId."';";
    // echo $sql1;

    if($deviceType == "SWITCH"){
        // Get action from SWITCH value
        if($_POST['value'] == '1')
            $action = 'on';
        else
            $action = 'off';

        // Encode JSON for Python module
        $iotJson = new \stdClass();
        $iotJson->deviceId = $deviceId;
        $iotJson->deviceType = $deviceType;
        $iotJson->action = $action;
        
    }
    else if($deviceType == "RGB LED"){
        // Get RGB Value from HEX color coding
        list($R, $G, $B) = sscanf($_POST['value'], "#%02x%02x%02x");

        // Encode JSON for Python module
        
        $iotJson = new \stdClass();
        $iotJson->deviceId = $deviceId;
        $iotJson->deviceType = $deviceType;
        $iotJson->action = 'on';
        $iotJson->R = $R;
        $iotJson->G = $G;
        $iotJson->B = $B;

    }
    $command_exec = 'python3 ../MQTT/openiot_device_actions.py "'. addcslashes(json_encode($iotJson),'"').'"';
    preg_replace('/\\\\/', '\\', $command_exec);
    
    $command = escapeshellcmd("python3 ../MQTT/test.py 2>&1");
    $str_output = shell_exec($command_exec." 2>&1");
    echo $str_output;
    mysqli_query($conn, $sql1);
    
}

if(isset($_POST['loadValues'])){
    $sql1 = "SELECT * FROM iot WHERE deviceId IN (SELECT deviceId from devices)";
    $result = mysqli_query($conn, $sql1);

    while($row = mysqli_fetch_assoc($result))
    {
        $finalArray[] = $row;
    }
    echo json_encode($finalArray);
}

if(isset($_POST['getHistory'])){
    if($deviceType == "SENSOR")
        $sql = "SELECT timestamp, value FROM iot_history WHERE deviceId='".$_POST['deviceId']."'&& eventType='".$_POST['eventType']."';";
    else
        $sql = "SELECT timestamp, value FROM iot_history WHERE deviceId='".$_POST['deviceId']."';";
    
    $result = mysqli_query($conn, $sql);
    // echo $sql;
    while($row = mysqli_fetch_assoc($result))
    {
        $valueArray[] = $row;
    }
    echo json_encode($valueArray);
}

if(isset($_POST['getOtaDetails'])){
    $deviceId = $_POST['deviceId'];
    $sql = "SELECT * FROM ota where deviceId='$deviceId' AND deviceId in 
                            (SELECT deviceId from devices where user_email='".$_SESSION['email']."');";
    $result = mysqli_query($conn, $sql);
    if($result){
        while($row = mysqli_fetch_assoc($result)){
            $output[] = $row;
        }
    }
    echo json_encode($output);
}

if(isset($_POST['ota'])){
    $target_file = "../Firmware/binaries/";
    $deviceId = $_POST['deviceId'];
    $filename = $_POST['filepath'];

    $deviceType = $_POST['deviceType'];
    $fullFilePath = $target_file . $filename;
    $newVersion = $_POST['new_version'];
    $scehduledDate = $_POST['scheduledDate'];

    $sql = "INSERT INTO ota (deviceId, new_version, filepath, start_time, status) VALUES (
        '$deviceId' ,
        '$newVersion' ,
        '$fullFilePath' ,
        '$scehduledDate' ,
        'scheduled'   
    );";

    // echo $sql;
    $result = mysqli_query($conn, $sql);

    if($result){
        $myobj = new \stdClass();
        $myobj->status = "true";
        $myobj->msg = "OTA Scheduled at $scheduledDate";
    }
    else{
        $myobj = new \stdClass();
        $myobj->status = "false";
        $myobj->msg = "Something went wrong.. please try again!!";
    }
    echo json_encode($myobj);
    return;
}
if(isset($_POST['getDetails'])){
    $deviceId = $_POST['deviceId'];
    $sql = "SELECT * FROM iot WHERE deviceId='$deviceId';";
    $result = mysqli_query($conn, $sql);
    while($row = mysqli_fetch_assoc($result))
    {
        $valueArray[] = $row;
    }
    echo json_encode($valueArray);
}

if(isset($_POST['redirectForDetails'])){
    $sql = "SELECT deviceId, deviceType, eventType from iot where deviceId='".$_POST['deviceId']."';";
    $result = mysqli_query($conn, $sql);
    $row = mysqli_fetch_assoc($result);
    // header("Location: details.php?deviceId=".$row['deviceId']."&&deviceType=".$row['deviceType']."&&eventType=".$row['eventType']);
    echo "details.php?deviceId=".$row['deviceId']."&&deviceType=".$row['deviceType']."&&eventType=".$row['eventType'];
}

if(isset($_POST['createRule'])){
    $inputDevice = $_POST['inputDevice'];
    $eventType = $_POST['eventType'];
    $condition = $_POST['condition'];
    $threshold = $_POST['threshold'];
    $outputDevice = $_POST['outputDevice'];
    $switchPosition = $_POST['switchPosition'];
    $brightness = $_POST['brightness'];
    $title = $_POST['title'];

    // $response = Array();
    $rule = Array();
    $data = $tempArray = Array();

    if(($inp = file_get_contents('../rule_config.json')) != false){
        $tempArray = json_decode($inp, true);
    }

    $tempArray[] = array(
        'title' => $title,
        'inputDevice' => $inputDevice,
        'eventType' => $eventType,
        'condition' => $condition,
        'threshold' => $threshold,
        'outputDevice' => $outputDevice,
        'switchPosition' => $switchPosition,
        'brightness' => $brightness,
        'active' => "1"
    );

    echo json_encode($tempArray);
    $jsonRule = json_encode($tempArray, JSON_PRETTY_PRINT);
    $file = fopen('../rule_config.json','w+a');
    fwrite($file, $jsonRule);
    fclose($file);
}

if(isset($_POST['readRules'])){
    if(($inp = file_get_contents('../rule_config.json')) != false){
        $tempArray = json_decode($inp, true);
    }
    echo count($tempArray);
}

if(isset($_POST['modifyJson'])){
    echo json_encode($_POST["data"]);
    $jsonRule = json_encode($_POST["data"], JSON_PRETTY_PRINT);
    $file = fopen('../rule_config.json','w+a');
    fwrite($file, $jsonRule);
    fclose($file);
}
die();
?>