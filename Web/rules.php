<?php
session_start();
include "db_config.php";

// If session is not active, these internal pages can not be accessible
if(!isset($_SESSION['active'])){
    header("Location: error404.php");
    die();
}
$_SESSION['2last_url'] = isset($_SESSION['last_url']) ? $_SESSION['last_url'] : null;
$_SESSION['last_url'] = $_SERVER['HTTP_REFERER'];

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Rules</title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
  <!-- daterange picker -->
  <link rel="stylesheet" href="plugins/daterangepicker/daterangepicker.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css">
  <!-- Tempusdominus Bootstrap 4 -->
  <link rel="stylesheet" href="plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="plugins/select2/css/select2.min.css">
  <link rel="stylesheet" href="plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
  <!-- Bootstrap4 Duallistbox -->
  <link rel="stylesheet" href="plugins/bootstrap4-duallistbox/bootstrap-duallistbox.min.css">
  <!-- BS Stepper -->
  <link rel="stylesheet" href="plugins/bs-stepper/css/bs-stepper.min.css">
  <!-- dropzonejs -->
  <link rel="stylesheet" href="plugins/dropzone/min/dropzone.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/adminlte.min.css">
  <link rel="stylesheet" href="plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css">
  <!-- Toastr -->
  <link rel="stylesheet" href="plugins/toastr/toastr.min.css">
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
      
    </ul>


  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index3.html" class="brand-link">
      <img src="dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light">OpenIOT</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user (optional) -->
      <?php
        include "sidebar.php";
      ?>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item">
            <a href="dashboard.php" class="nav-link">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
                
              </p>
            </a>
          </li>
         
          <li class="nav-item ">
            <a href="details.php" class="nav-link">
              <i class="nav-icon fas fa-chart-pie"></i>
              <p>
                Device Details
              </p>
            </a>
            
          </li>
          <li class="nav-item">
            <a href="rules.php" class="nav-link active">
              <i class="nav-icon fas fa-edit"></i>
              <p>
                Automation Rules
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="contributors.php" class="nav-link">
              <i class="nav-icon fas fa-user-secret"></i>
              <p>
                Contributors
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-user-lock"></i>
              <p>
                User Profile
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="lockscreen.php" class="nav-link">
                  <i class="fas fa-lock nav-icon"></i>
                  <p>Lockscreen</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="profile.php" class="nav-link">
                  <i class="fas fa-user-alt nav-icon"></i>
                  <p>Profile</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="logout.php" class="nav-link">
                  <i class="fas fa-sign-out-alt nav-icon"></i>
                  <p>Logout</p>
                </a>
              </li>
            </ul>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Automation Rules</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="dashboard.php">Home</a></li>
              <li class="breadcrumb-item active">Automation Rules</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
           <!-- SELECT2 EXAMPLE -->
        <div class="card card-default card-primary">
          <div class="card-header">
            <h3 class="card-title">Create New Rule</h3>

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse">
                <i class="fas fa-minus"></i>
              </button>
              <button type="button" class="btn btn-tool" data-card-widget="remove">
                <i class="fas fa-times"></i>
              </button>
            </div>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
          <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                  <label>Enter Title For Your Rule: </label>
                  <input id="ruleTitle" type="text" required class="form-control form-control-border" style="width: 100%;" placeholder="Enter Title">
                </div>
              </div>
          </div>
          <!-- <div class="row">
            <h3> Configuration: </h3>
          </div> -->
            <div class="row">
              <div class="col-md-2">
                <div class="form-group">
                  <label>Input Device</label>
                  <select id="inputDevice" class="form-control select2bs4" style="width: 100%;" required>
                    <option selected="selected">Select Device</option>
                        <?php
                        // Get device list from database for logged in user
                        $sql = "SELECT deviceId from iot WHERE deviceType='SENSOR' && deviceId in 
                                (SELECT deviceId from devices WHERE user_email='".$_SESSION['email']."');";
                        $result = mysqli_query($conn, $sql);
                        while($row = mysqli_fetch_assoc($result)){
                        ?>
                        <option><?php echo $row['deviceId']; ?></option>
                        <?php
                        }  // End of While loop
                        ?>
                  </select>
                </div>
              </div>

              <div class="col-md-2">
                <div class="form-group">
                    <label>Event Type</label>
                    <select id="eventType" required class="form-control select2bs4" style="width: 100%;" data-placeholder="Select Condition">
                        <option>TEMPERATURE</option>
                        <option>HUMIDITY</option>
                        <option>MOTION</option>
                        
                    </select>
                    </div>
              </div>
        
              <div class="col-md-2">
                <div class="form-group">
                    <label>Condition</label>
                    <select id="condition" required class="form-control select2bs4" style="width: 100%;" data-placeholder="Select Condition">
                        <option>==</option>
                        <option>>=</option>
                        <option><=</option>
                        <option><</option>
                        <option>></option>
                        <option>!=</option>
                    </select>
                </div>
              </div>
              <div class="col-md-1">
                <div class="form-group">
                  <label>Threshold</label>
                  <input id="threshold" required type="text" class="form-control" style="width: 100%;" placeholder="Value">
                </div>
              </div>
              <div class="col-md-2">
                <div class="form-group">
                  <label>Output Device</label>
                  <select id="outputDevice" required class="form-control select2bs4" style="width: 100%;">
                    <option selected="selected">Select Device</option>
                        <?php
                        // Get device list from database for logged in user
                        $sql = "SELECT deviceId from iot WHERE deviceType='SWITCH' && deviceId in 
                                (SELECT deviceId from devices WHERE user_email='".$_SESSION['email']."');";
                        $result = mysqli_query($conn, $sql);
                        while($row = mysqli_fetch_assoc($result)){
                        ?>
                        <option><?php echo $row['deviceId']; ?></option>
                        <?php
                        }  // End of While loop
                        ?>
                  </select>
                </div>
                
              </div>
              <div class="col-md-1">
                <div class="form-group">
                    <label>ON/OFF</label>
                    <select id="switchPosition" required onchange="disableBrightness()" class="form-control select2bs4" style="width: 100%;" data-placeholder="Select Condition">
                        <option>ON</option>
                        <option>OFF</option>
                    </select>
                </div>
                
              </div>
              <div class="col-md-2">
                <div class="form-group">
                  <label>Brightness</label>
                  <input id="brightness" required type="text" class="form-control" style="width: 100%;" placeholder="Value">
                </div>
                <button type="submit" onclick="submitData()" class="btn btn-success col cancel">
                        <i class="fas fa-plus"></i>
                        <span>Create Rule</span>
                </button>
              </div>
            </div>
        </div>
           
          <!-- /.card-body -->
      </div>
        <!-- SELECT2 EXAMPLE -->
        <?php 
        if(file_exists('../rule_config.json')){
          $inp = file_get_contents('../rule_config.json');
        }
        else{
          $inp = "";
        }
        $tempArray = json_decode($inp, true);
        $i = 0;
        $j = 0;
        $devices = array();
        $sql = "SELECT deviceId from devices where user_email='".$_SESSION['email']."';";
        $result = mysqli_query($conn, $sql);
        while($row = mysqli_fetch_assoc($result)){
            $devices[] = $row['deviceId'];
        }
        // print_r($devices);
        
        while($inp != false && $i < count($tempArray)){
            $json = [];
            // print_r($json);
            $json = $tempArray[$i];
            // echo $json['active'];
            if(in_array($json['outputDevice'], $devices) == false || $json['active']==0){
                
                $i += 1;
                continue;
            }
        ?>
        <div class="card card-default">
          <div class="card-header">
            <h3 class="card-title">Rule: #<?php echo ($j+1). " <b> Title: " .$json['title']; ?></b></h3>

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse">
                <i class="fas fa-minus"></i>
              </button>
              <button type="button" class="btn btn-tool" data-card-widget="remove">
                <i class="fas fa-times"></i>
              </button>
              <button type="button" class="btn btn-tool" onclick="deleteRecord(<?php echo $i; ?>)" data-card-widget="delete">
              <i class="fas fa-trash"></i>
              </button>
            </div>
          </div>
          <!-- /.card-header -->
          <div class="card-body">

            <div class="row">
            <i class="fas fa-flag"></i> &nbsp;
              If <?php echo strtolower($json['eventType']);?> of device <?php echo $json['inputDevice']; ?> 
              is <?php if($json['condition']== "==") echo "equal to ";
                        else if($json['condition']== "<=") echo "less than or equal to ";
                        else if($json['condition']== ">=") echo "greater than or equal to ";
                        else if($json['condition']== "!=") echo "not equal to ";
                        else if($json['condition']== "<") echo "less than ";
                        if($json['condition']== ">") echo "greater than ";
              
                        echo $json['threshold']; 
              ?> then 

              <?php if($json['switchPosition']=="OFF"){
                  echo "turn off switch ". $json['outputDevice']; 
                }
                else{
                    echo "turn on switch ". $json['outputDevice']. " with brightness level ".$json['brightness']; 
                }
                ?>
              
            </div>
          </div>
          <!-- /.card-body -->
        </div>
        <?php $i += 1; $j += 1;
        }
        ?>
        <!-- /.card -->
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
   
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- Select2 -->
<script src="plugins/select2/js/select2.full.min.js"></script>
<!-- Bootstrap4 Duallistbox -->
<script src="plugins/bootstrap4-duallistbox/jquery.bootstrap-duallistbox.min.js"></script>
<!-- InputMask -->
<script src="plugins/moment/moment.min.js"></script>
<script src="plugins/inputmask/jquery.inputmask.min.js"></script>
<!-- date-range-picker -->
<script src="plugins/daterangepicker/daterangepicker.js"></script>
<!-- bootstrap color picker -->
<script src="plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
<!-- Bootstrap Switch -->
<script src="plugins/bootstrap-switch/js/bootstrap-switch.min.js"></script>
<!-- BS-Stepper -->
<script src="plugins/bs-stepper/js/bs-stepper.min.js"></script>
<!-- dropzonejs -->
<script src="plugins/dropzone/min/dropzone.min.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<script src="plugins/sweetalert2/sweetalert2.min.js"></script>
<script src="plugins/toastr/toastr.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
<!-- Page specific script -->
<script>
    function expandRules(data){
        console.log(data.length);
    }
    $(document).ready(function(){
        $.ajax({
            type: "POST",
            url: "updateDevice.php",
            data: ({
                readRules:1
            }),
            cache: false,
            success: function(response) {
                console.log(response);
                // data = JSON.parse(response);
                //expandRules(data);
            }
            });
    })
  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Initialize Select2 Elements
    $('.select2bs4').select2({
      theme: 'bootstrap4'
    })

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date picker
    $('#reservationdate').datetimepicker({
        format: 'L'
    });

    //Date and time picker
    $('#reservationdatetime').datetimepicker({ icons: { time: 'far fa-clock' } });

    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({
      timePicker: true,
      timePickerIncrement: 30,
      locale: {
        format: 'MM/DD/YYYY hh:mm A'
      }
    })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )

    //Timepicker
    $('#timepicker').datetimepicker({
      format: 'LT'
    })

    //Bootstrap Duallistbox
    $('.duallistbox').bootstrapDualListbox()

    //Colorpicker
    $('.my-colorpicker1').colorpicker()
    //color picker with addon
    $('.my-colorpicker2').colorpicker()

    $('.my-colorpicker2').on('colorpickerChange', function(event) {
      $('.my-colorpicker2 .fa-square').css('color', event.color.toString());
    })

    $("input[data-bootstrap-switch]").each(function(){
      $(this).bootstrapSwitch('state', $(this).prop('checked'));
    })
  })
  // BS-Stepper Init
  document.addEventListener('DOMContentLoaded', function () {
    window.stepper = new Stepper(document.querySelector('.bs-stepper'))
  })

  // DropzoneJS Demo Code Start
  Dropzone.autoDiscover = false

  // Get the template HTML and remove it from the doumenthe template HTML and remove it from the doument
  var previewNode = document.querySelector("#template")
  previewNode.id = ""
  var previewTemplate = previewNode.parentNode.innerHTML
  previewNode.parentNode.removeChild(previewNode)

  var myDropzone = new Dropzone(document.body, { // Make the whole body a dropzone
    url: "/target-url", // Set the url
    thumbnailWidth: 80,
    thumbnailHeight: 80,
    parallelUploads: 20,
    previewTemplate: previewTemplate,
    autoQueue: false, // Make sure the files aren't queued until manually added
    previewsContainer: "#previews", // Define the container to display the previews
    clickable: ".fileinput-button" // Define the element that should be used as click trigger to select files.
  })

  myDropzone.on("addedfile", function(file) {
    // Hookup the start button
    file.previewElement.querySelector(".start").onclick = function() { myDropzone.enqueueFile(file) }
  })

  // Update the total progress bar
  myDropzone.on("totaluploadprogress", function(progress) {
    document.querySelector("#total-progress .progress-bar").style.width = progress + "%"
  })

  myDropzone.on("sending", function(file) {
    // Show the total progress bar when upload starts
    document.querySelector("#total-progress").style.opacity = "1"
    // And disable the start button
    file.previewElement.querySelector(".start").setAttribute("disabled", "disabled")
  })

  // Hide the total progress bar when nothing's uploading anymore
  myDropzone.on("queuecomplete", function(progress) {
    document.querySelector("#total-progress").style.opacity = "0"
  })

  // Setup the buttons for all transfers
  // The "add files" button doesn't need to be setup because the config
  // `clickable` has already been specified.
  document.querySelector("#actions .start").onclick = function() {
    myDropzone.enqueueFiles(myDropzone.getFilesWithStatus(Dropzone.ADDED))
  }
  document.querySelector("#actions .cancel").onclick = function() {
    myDropzone.removeAllFiles(true)
  }

  // DropzoneJS Demo Code End



  function submitData(){
        var Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000
        });
      var inputDevice = document.getElementById("inputDevice").value;
      var eventType = document.getElementById("eventType").value;
      var condition = document.getElementById("condition").value;
      var threshold = document.getElementById("threshold").value;
      var outputDevice = document.getElementById("outputDevice").value;
      var switchPosition = document.getElementById("switchPosition").value;
      var brightness = document.getElementById("brightness").value;
      var title = document.getElementById("ruleTitle").value;
      if(title == ""){
        Toast.fire({
                    icon: 'warning',
                    title: '&nbsp; Enter Rule Name'
                })
                return;
      }

      if(inputDevice == "Select Device"){
        Toast.fire({
                    icon: 'warning',
                    title: '&nbsp; Select Input Device ID'
                })
                return;
      }
      if(outputDevice == "Select Device"){
        Toast.fire({
                    icon: 'warning',
                    title: '&nbsp; Select Output Device ID'
                })
                return;
      }
      if(threshold == ""){
        Toast.fire({
                    icon: 'warning',
                    title: '&nbsp; Enter Threshold value'
                })
                return;
      }
      if(brightness == "" && switchPosition=="ON"){
        Toast.fire({
                    icon: 'warning',
                    title: '&nbsp; Enter Brightness value'
                })
                return;
      }
      $.ajax({
            type: "POST",
            url: "updateDevice.php",
            data: ({
                createRule: 1,
                inputDevice: inputDevice,
                eventType: eventType,
                condition: condition,
                threshold: threshold,
                outputDevice: outputDevice,
                switchPosition: switchPosition,
                brightness: brightness,
                title: title
            }),
            cache: false,
            success: function(response) {
                Toast.fire({
                    icon: 'success',
                    title: 'Rule:' + title +' Created Successfully For DeviceId: '+ inputDevice
                })
            }
          });
  }
  function disableBrightness(){
      if(document.getElementById("switchPosition").value == "OFF")
        document.getElementById('brightness').disabled = true;
      else
        document.getElementById('brightness').disabled = false;
  }

    function readTextFile(file, callback) {
        var rawFile = new XMLHttpRequest();
        rawFile.overrideMimeType("application/json");
        rawFile.open("GET", file, true);
        rawFile.onreadystatechange = function() {
            if (rawFile.readyState === 4 && rawFile.status == "200") {
                callback(rawFile.responseText);
            }
        }
        rawFile.send(null);
    }

  function deleteRecord(id){
    var Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000
        });
    //   alert(id)
      readTextFile("../rule_config.json", function(text){
        var data = JSON.parse(text);
        data[id].active=0;
        console.log(data);
        $.ajax({
            type: "POST",
            url: "updateDevice.php",
            data: ({
                data: data,
                modifyJson: 1
            }),
            cache: false,
            success: function(response) {
                Toast.fire({
                    icon: 'success',
                    title: 'Rule: #'+(id+1)+ 'set to inactive'
                })
            }
          });
    })
}

</script>
</body>
</html>
