## Create a ```openiot``` database and select it for further openrations
```
CREATE DATABASE openiot;
use openiot;
```

## Create ```users``` table

```
CREATE TABLE users (
id INT(6) UNSIGNED,
email VARCHAR(50) PRIMARY KEY,
firstname VARCHAR(30) NOT NULL,
lastname VARCHAR(30),
gender VARCHAR(6),
password VARCHAR(20),
picture VARCHAR(200),
reg_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
regType VARCHAR(10)
)

mysql> desc users;
+-----------+-----------------+------+-----+---------------------+-------------------------------+
| Field     | Type            | Null | Key | Default             | Extra                         |
+-----------+-----------------+------+-----+---------------------+-------------------------------+
| id        | int(6) unsigned | YES  |     | NULL                |                               |
| email     | varchar(50)     | NO   | PRI | NULL                |                               |
| firstname | varchar(30)     | NO   |     | NULL                |                               |
| lastname  | varchar(30)     | YES  |     | NULL                |                               |
| gender    | varchar(6)      | YES  |     | NULL                |                               |
| picture   | varchar(200)    | YES  |     | NULL                |                               |
| reg_date  | timestamp       | NO   |     | current_timestamp() | on update current_timestamp() |
| password  | varchar(20)     | YES  |     | NULL                |                               |
| regType   | varchar(10)     | YES  |     | NULL                |                               |
+-----------+-----------------+------+-----+---------------------+-------------------------------+
7 rows in set (0.00 sec)
```

### Check the entries in users table
```
Select * from users;
```

## devices table
```
mysql> CREATE TABLE devices ( deviceId varchar(15) PRIMARY KEY, user_email varchar(30), timestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP );

mysql> desc devices;
+------------+-------------+------+-----+---------------------+-------------------------------+
| Field      | Type        | Null | Key | Default             | Extra                         |
+------------+-------------+------+-----+---------------------+-------------------------------+
| deviceId   | varchar(15) | NO   | PRI | NULL                |                               |
| user_email | varchar(30) | YES  |     | NULL                |                               |
| timestamp  | timestamp   | NO   |     | current_timestamp() | on update current_timestamp() |
+------------+-------------+------+-----+---------------------+-------------------------------+
3 rows in set (0.00 sec)
```

## IoT table
```
mysql>  CREATE TABLE iot ( deviceId varchar(50) PRIMARY KEY, deviceType ENUM('SENSOR', 'SWITCH', 'RGB LED'), eventType ENUM('TEMPERATURE', 'MOTION', 'HUMIDITY'), value varchar(20), timestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP, last_heard TIMESTAMP DEFAULT CURRENT_TIMESTAMP);

mysql> desc iot;
+------------+-----------------------------------------+------+-----+---------------------+-------+
| Field      | Type                                    | Null | Key | Default             | Extra |
+------------+-----------------------------------------+------+-----+---------------------+-------+
| deviceId   | varchar(50)                             | NO   | PRI | NULL                |       |
| deviceType | enum('SENSOR','SWITCH', 'RGB LED')      | YES  |     | NULL                |       |
| eventType  | enum('TEMPERATURE','MOTION','HUMIDITY') | YES  |     | NULL                |       |
| value      | varchar(20)                             | YES  |     | NULL                |       |
| timestamp  | timestamp                               | NO   |     | current_timestamp() |       |
| last_heard | timestamp                               | NO   |     | current_timestamp() |       |
+------------+-----------------------------------------+------+-----+---------------------+-------+
6 rows in set (0.01 sec)
```

## Create a iot_history table
```
mysql> CREATE TABLE iot_history ( deviceId varchar(50), deviceType ENUM('SENSOR', 'SWITCH','RGB LED'), eventType ENUM('TEMPERATURE', 'MOTION', 'HUMIDITY'), value varchar(20), timestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP);

mysql> desc iot_history;
+------------+-----------------------------------------+------+-----+---------------------+-------+
| Field      | Type                                    | Null | Key | Default             | Extra |
+------------+-----------------------------------------+------+-----+---------------------+-------+
| deviceId   | varchar(50)                             | YES  |     | NULL                |       |
| deviceType | enum('SENSOR','SWITCH', 'RGB LED')      | YES  |     | NULL                |       |
| eventType  | enum('TEMPERATURE','MOTION','HUMIDITY') | YES  |     | NULL                |       |
| value      | varchar(20)                             | YES  |     | NULL                |       |
| timestamp  | timestamp                               | NO   |     | current_timestamp() |       |
+------------+-----------------------------------------+------+-----+---------------------+-------+
5 rows in set (0.13 sec)
```

## Trigger to update history table after updating the value in iot table
```
DELIMITER $$  
  
CREATE TRIGGER after_iot_update  
AFTER UPDATE  
ON iot FOR EACH ROW  
BEGIN  
    INSERT into iot_history (deviceId, deviceType, eventType, value) VALUES (new.deviceId, new.deviceType, new.eventType, new.value);  
END $$  
  
DELIMITER ; 
```

## OTA Table
```
mysql> CREATE TABLE ota ( deviceId varchar(50), curr_version varchar(20), new_version varchar(20), filepath varchar(50), start_time varchar(50), status varchar(10));

mysql> desc ota;
+--------------+-------------+------+-----+---------+-------+
| Field        | Type        | Null | Key | Default | Extra |
+--------------+-------------+------+-----+---------+-------+
| deviceId     | varchar(50) | YES  |     | NULL    |       |
| curr_version | varchar(20) | YES  |     | NULL    |       |
| new_version  | varchar(20) | YES  |     | NULL    |       |
| filepath     | varchar(50) | YES  |     | NULL    |       |
| start_time   | varchar(50) | YES  |     | NULL    |       |
| status       | varchar(10) | YES  |     | NULL    |       |
+--------------+-------------+------+-----+---------+-------+
6 rows in set (0.01 sec)
```
