<?php
session_start();
require 'vendor/autoload.php';
include ("oauth2_config.php");
include ("db_config.php");

$_SESSION['2last_url'] = $_SESSION['last_url'];
$_SESSION['last_url'] = $_SERVER['HTTP_REFERER'];

if(isset($_POST['deleteDevice'])){
  $sql = "DELETE FROM devices WHERE deviceId='".$_POST['deviceId']."';";
  mysqli_query($conn, $sql);
}
if(isset($_GET['code'])){

  $token = $google_client->fetchAccessTokenWithAuthCode($_GET['code']);
  if(!isset($token['error'])){
    $google_client->setAccessToken($token['access_token']);
    $_SESSION['access_token'] = $token['access_token'];
    
    $googe_service = new Google_Service_Oauth2($google_client);

    $data = $googe_service->userinfo->get();

    if(!empty($data['given_name']))
    {
       $_SESSION['first_name'] = $data['given_name'];
       $_SESSION['name'] = $_SESSION['first_name'];
    }
    
    if(!empty($data['family_name']))
    {
       $_SESSION['last_name'] = $data['family_name'];
       $_SESSION['name'] .= " ".$_SESSION['last_name'];
    }

    if(!empty($data['email']))
    {
       $_SESSION['email'] = $data['email'];
    }

    if(!empty($data['gender']))
    {
       $_SESSION['gender'] = $data['gender'];
    }

    if(!empty($data['picture']))
    {
       $_SESSION['picture'] = $data['picture'];
    }
  }
  $_SESSION['active'] = "true";
  // check if user is already present in databse, else add user;

  $sql = "SELECT * from users where email='".$_SESSION['email']."';";
  $result = mysqli_query($conn, $sql);
  
  if(mysqli_num_rows($result) == 0){
    $sql = "INSERT INTO users (email, firstname, lastname, gender, picture, regType) VALUES
          (
            '".$_SESSION['email']."',
            '".$_SESSION['first_name']."',
            '".$_SESSION['last_name']."',
            '".$_SESSION['gender']."',
            '".$_SESSION['picture']."',
            'OAuth'
          );";
    
    if (mysqli_query($conn, $sql));
  }

  //TODO: To change this location after hosting on server
  header("Location: dashboard.php");
}

// If session is not active, these internal pages can not be accessible
if(!isset($_SESSION['active'])){
  header("Location: error404.php");
  die();
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>OpenIoT Home</title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Tempusdominus Bootstrap 4 -->
  <link rel="stylesheet" href="plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- JQVMap -->
  <link rel="stylesheet" href="plugins/jqvmap/jqvmap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/adminlte.min.css">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="plugins/daterangepicker/daterangepicker.css">
  <!-- summernote -->
  <link rel="stylesheet" href="plugins/summernote/summernote-bs4.min.css">
  <link rel="stylesheet" href="plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
  <link rel="stylesheet" href="plugins/datatables-buttons/css/buttons.bootstrap4.min.css">

  <link rel="stylesheet" href="plugins/select2/css/select2.min.css">
  <link rel="stylesheet" href="plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">

  <link rel="stylesheet" href="plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css">
  <!-- Toastr -->
  <link rel="stylesheet" href="plugins/toastr/toastr.min.css">

</head>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>

    </ul>

  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index3.html" class="brand-link">
      <img src="dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light">OpenIoT</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <?php
        include "sidebar.php";
      ?>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item">
            <a href="dashboard.php" class="nav-link active">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
              </p>
            </a>
          </li>
         
          <li class="nav-item">
            <a href="details.php" class="nav-link">
              <i class="nav-icon fas fa-chart-pie"></i>
              <p>
                Device Details
              </p>
            </a>
            
          </li>
          <li class="nav-item">
            <a href="rules.php" class="nav-link">
              <i class="nav-icon fas fa-edit"></i>
              <p>
                Automation Rules
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="contributors.php" class="nav-link">
              <i class="fas fa-user-secret nav-icon "></i>
              <p>
                Contributors
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-user-lock"></i>
              <p>
                User Profile
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="lockscreen.php" class="nav-link">
                  <i class="fas fa-lock nav-icon"></i>
                  <p>Lockscreen</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="profile.php" class="nav-link">
                  <i class="fas fa-user-alt nav-icon"></i>
                  <p>Profile</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="logout.php" class="nav-link">
                  <i class="fas fa-sign-out-alt nav-icon"></i>
                  <p>Logout</p>
                </a>
              </li>
            </ul>
          </li>

        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Dashboard</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
         
      </div>

      <div class="row">
        <section class="content col-lg-12 connectedSortable">
          <div class="container-fluid">
            <div class="row">
              <div class="col-md-12">
                <div class="card card-primary">
                  <div class="card-header">
                    <h3 class="card-title">Available Devices</h3>
                  </div>
                  <!-- /.card-header -->
                  <div class="card-body">
                    <table id="example1" class="table ">
                        <?php 
                          $sql = "SELECT * FROM iot WHERE deviceId IN (SELECT deviceId from devices where user_email='".$_SESSION['email']."');";
                          $result = mysqli_query($conn, $sql);
                          $i = 1;
                          if(mysqli_num_rows($result) == 0){
                            ?> <H2> No Devices Available... </H2> <?php
                          }
                          else{
                            ?>
                            <thead>
                              <tr>
                                <th style="width: 2%">#</th>
                                <th style="width: 2%">Device Type</th>
                                <th style="width: 5%">Device Id</th>
                                <th style="width: 5%">EventType</th>
                                <th style="width: 2%">Value</th>
                                <th style="width: 3%">Status</th>
                                <th style="width: 5%">Last Updated</th>
                                <th style="width: 1%">History</th>
                                <th style="width: 1%">Act</th>
                              </tr>
                            </thead>
                            <tbody>
                            <?php
                          }
                          while($row = mysqli_fetch_assoc($result)){
                        ?>
                        
                        <?php 
                                $deviceId = $row['deviceId'];
                                $eventType = $row['eventType'];
                                $deviceType = $row['deviceType'];
                                $link = "details.php?deviceId=".$deviceId."&&deviceType=".$deviceType."&&eventType=".$eventType;
                        ?>
                        <tr>
                          <td class="py-0 align-middle" class="py-0 align-middle"><?php echo $i ?></td>
                          <?php
                            if($row['deviceType']=="SENSOR"){
                          ?>
                              <td style="height:20%" class="py-0 align-middle"><center><img src="images/sensor_1.png" width="40%"></center></td>
                          <?php
                            }
                            else if($row['deviceType']=="SWITCH"){
                              $id = "deviceIcon_".$row['deviceId'];
                              if($row['value']==1){
                          ?>
                            <td style="height:20%" class="py-0 align-middle"><center><img src="images/switch-on.png" onclick="switchChange(this.id, '<?php echo $deviceType;?>')" id="<?php echo $id; ?>" width="50%"></center></td>
                              <?php }
                              else { 
                              ?>
                              <td style="height:20%" class="py-0 align-middle"><center><img src="images/switch-off.png" onclick="switchChange(this.id, '<?php echo $deviceType;?>')" id="<?php echo $id; ?>" width="50%"></center></td>
                            
                            <?php } 
                              }
                              else if($row['deviceType'] == "RGB LED"){ 
                              $ledIconId = "ledId_" . $row['deviceId'];
                              $colorPalletId = "ledColor_" . $row['deviceId'];
                            ?>
                            <td class="py-0 align-middle"><center> <i class="far fa-lightbulb fa-2x" style="color:<?php echo $row['value']; ?>" id="<?php echo $ledIconId;?>"></i> <input type="color" id="<?php echo $colorPalletId; ?>" value="<?php echo $row['value']; ?>" onchange="addColor('<?php echo $ledIconId; ?>', '<?php echo $colorPalletId; ?>')"> </center></td>
                            <?php } ?>

                          <td class="py-0 align-middle">
                             <a href="#" onclick="loadModal('<?php echo $row['deviceId']; ?>')"> <?php echo $row['deviceId']; ?> </a>
                          </td>
                          <?php
                          if($row['deviceType']=="SWITCH"){
                          ?>
                          <td class="py-0 align-middle"> N.A. </td>
                          <?php 
                          } 
                          else {
                          ?>
                          <td class="py-0 align-middle">  <?php echo $row['eventType']; ?></td>
                          <?php } ?>
                          <?php $valueId=$row['deviceId']."_id";?>
                          <td class="py-0 align-middle" id='<?php echo $valueId; ?>'><?php echo $row['value'];?></td>
                          <td class="py-0 align-middle"><span class="badge bg-success">Active</span> </td>
                          <td class="py-0 align-middle" id="<?php echo $row['deviceId']."_ts"; ?>">
                          <?php echo $row['timestamp']; ?>
                          </td>
                          
                          <td class="py-0 align-middle">
                              <a 
                                href="<?php echo $link; ?>"> 
                                Get Details 
                              </a>
                          </td>
                          <td >
                          <a href="#" class="btn btn-danger " id="<?php echo $row['deviceId'];?>" onclick="deleteDevice(this.id)"><i class="fas fa-trash"></i></a>
                            
                          </td>
                        </tr>
                        <?php
                          $i = $i + 1;
                            }
                        ?>
                      </tbody>
                    </table>
                  </div>
                  <!-- /.card-body -->
                  
                </div>
              </div>
            </div>
          </div><!-- /.container-fluid -->
        </section>
        
        <section class="content col-lg-12">
          
            

        <div class="container-fluid">
           <!-- SELECT2 EXAMPLE -->
          <div class="card card-default card-primary">
            <div class="card-header">
              <h3 class="card-title">Add New Device</h3>

              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                  <i class="fas fa-minus"></i>
                </button>
                <button type="button" class="btn btn-tool" data-card-widget="remove">
                  <i class="fas fa-times"></i>
                </button>
              </div>
            </div>

            <div class="card-body">
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="add_deviceId">Device Mac Address</label>
                    <input type="text" class="form-control" name="deviceId" id="add_deviceId" placeholder="Device ID">
                  </div>
                  
                </div>

                <div class="col-md-6">
                  <div class="form-group">
                    <label> Select Device Type </label>
                    <select class="form-control select2bs4" style="width: 100%;" name="deviceType" id="add_deviceType">
                      <option selected="selected">Select Device type</option>
                      <option >Switch</option>
                      <option>Sensor</option>
                      <option>RGB LED</option>
                    </select>
                  </div>
                  <div class="form-group">
                    <button type="button" onclick="addDevice()" class="btn btn-success float-right">
                      <i class="fas fa-plus"></i>
                      <span>Add Device</span>
                    </button>
                  </div>
                </div>
                
              </div>
           </div>
           <!-- /.card-body -->
          </div>
        </div><!-- /.container-fluid -->
        </section>
      </div>
      </div>
    </section>
  </div>
  <div class="modal fade" id="modal-lg">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Device OTA Information</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                
              </button>
            </div>
            <div class="modal-body">
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Device ID</label>
                    <input type="text" class="form-control" disabled value="" id="modal_deviceId">
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="form-group">
                    <label>Device Type</label>
                    <input type="text" class="form-control" disabled value="" id="modal_deviceType">
                  </div>
                </div>

              </div>

              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Event Type</label>
                    <input type="text" class="form-control" disabled value="" id="modal_eventType">
                  </div>
                </div>

                <div class="col-md-3">
                  <div class="form-group">
                    <label>Current OTA Version</label>
                    <input type="text" class="form-control" disabled value="" id="modal_otaVersion">
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="form-group">
                    <label>New OTA Version</label>
                    <input type="text" class="form-control" value="" id="modal_newVersion">
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <!-- <div class="form-group">
                        <label>Schedule OTA</label>

                        <div class="input-group date" id="timepicker" data-target-input="nearest">
                          <input type="text" class="form-control datetimepicker" id="scheduledDate" data-target="#timepicker"/>
                          <div class="input-group-append" data-target="#timepicker" data-toggle="datetimepicker">
                              <div class="input-group-text"><i class="far fa-clock"></i></div>
                          </div>
                        </div>
                  </div> -->
                  <div class="form-group">
                  <label>Schedule OTA</label>

                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="far fa-clock"></i></span>
                    </div>
                    <input type="text" class="form-control float-right" id="reservationtime">
                  </div>
                  <!-- /.input group -->
                </div>
                </div>

                <div class="col-md-6">
                  <div class="form-group">
                        <label for="exampleInputFile">Select Binary File</label>
                        <div class="input-group">
                          <div class="custom-file">
                            <input type="file" class="custom-file-input binaryFile" id="exampleInputFile">
                            <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                          </div>
                          <div class="input-group-append">
                            <span class="input-group-text">Upload</span>
                          </div>
                        </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="button" onclick="startOTA()" class="btn btn-primary">Start OTA</button>
            </div>
            <div class="col-md-12" id="ota_table_div">
              
              <div class="card">
                <div class="card-header bg-primary">
                  <h3 class="card-title">Recent OTAs</h3>
                </div>
                
                <div class="card-body p-0">
                  <table class="table table-bordered" id="OtaTable">
                    <thead>
                      <tr>
                        <th style="width: 10%">#</th>
                        <th style="width: 15%">Version</th>
                        <th style="width: 30%">File Name</th>
                        <th style="width: 30%">Scheduled Time</th>
                        <th style="width: 15%">Status</th>
                      </tr>
                    </thead>
                    <tbody id="OtaTableBody">
                      
                    </tbody>
                  </table>
                </div>
                <!-- /.card-body -->
              </div>
            </div>
            
          </div>
        </div>
        <!-- /.modal-dialog -->
      </div>
  <?php
  include "footer.php";
  ?>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="plugins/jquery/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="plugins/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->

<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- ChartJS -->
<script src="plugins/chart.js/Chart.min.js"></script>
<!-- Sparkline -->
<script src="plugins/sparklines/sparkline.js"></script>
<!-- JQVMap -->
<script src="plugins/jqvmap/jquery.vmap.min.js"></script>
<script src="plugins/jqvmap/maps/jquery.vmap.usa.js"></script>
<!-- jQuery Knob Chart -->
<script src="plugins/jquery-knob/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="plugins/moment/moment.min.js"></script>
<script src="plugins/daterangepicker/daterangepicker.js"></script>

<!-- Tempusdominus Bootstrap 4 -->
<script src="plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
<!-- Summernote -->
<script src="plugins/summernote/summernote-bs4.min.js"></script>
<!-- overlayScrollbars -->
<script src="plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="dist/js/pages/dashboard.js"></script>

<script src="plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- DataTables  & Plugins -->
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script src="plugins/jszip/jszip.min.js"></script>
<script src="plugins/pdfmake/pdfmake.min.js"></script>
<script src="plugins/pdfmake/vfs_fonts.js"></script>
<script src="plugins/datatables-buttons/js/buttons.html5.min.js"></script>
<script src="plugins/datatables-buttons/js/buttons.print.min.js"></script>
<script src="plugins/datatables-buttons/js/buttons.colVis.min.js"></script>
<script src="plugins/bs-custom-file-input/bs-custom-file-input.min.js"></script>
<script src="plugins/moment/moment.min.js"></script>
<script src="plugins/inputmask/jquery.inputmask.min.js"></script>
<script src="plugins/sweetalert2/sweetalert2.min.js"></script>
<script src="plugins/select2/js/select2.full.min.js"></script>
<!-- date-range-picker -->
<script src="plugins/daterangepicker/daterangepicker.js"></script>
<script src="plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
<script>
  //color picker with addon
  
  $(function () {

    $("#example1").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false,
      "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });

    $('.select2').select2()

    //Initialize Select2 Elements
    $('.select2bs4').select2({
      theme: 'bootstrap4'
    })
   
    //Date picker
    $('#reservationdate').datetimepicker({
        format: 'L'
    });

    //Date and time picker
    $('#reservationdatetime').datetimepicker({ icons: { time: 'far fa-clock' } });

    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({
      timePicker: true,
      timePickerIncrement: 5,
      locale: {
        format: 'MM/DD/YYYY hh:mm A'
      }
    })
    
    //Timepicker
    $('#timepicker').datetimepicker({
      format: 'LT'
    })

  });
  $(function () {
    bsCustomFileInput.init();
  });

  function addColor(deviceLedId, colorPalletId){
    var color = document.getElementById(colorPalletId).value;
    // alert(id);
    document.getElementById(deviceLedId).style.color=color;
    $.ajax({
            type: "POST",
            url: "updateDevice.php",
            data: ({
                update: 1,
                deviceId: deviceLedId,
                deviceType: "RGB LED",
                value: color
            }),
            cache: false,
            success: function(data) {
              // console.log(data);
            }
        });

  }

  function addDevice(){
    var deviceId = document.getElementById('add_deviceId').value;
    var deviceType = document.getElementById('add_deviceType').value;

    var Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000
    });

    if(deviceId == ""){
      Toast.fire({
        icon: 'error',
        title: ' deviceId can not be empty'
      })
      return;
    }
    if(deviceType == ""){
      Toast.fire({
        icon: 'error',
        title: ' deviceType can not be empty'
      })
      return;
    }

    $.ajax({
            type: "POST",
            url: "updateDevice.php",
            data: ({
                addDevice: 1,
                deviceId: deviceId,
                deviceType: deviceType
            }),
            cache: false,
            success: function(response) {
              var data = JSON.parse(response);
              if(data['status'] == 'true'){
                Toast.fire({
                      icon: 'success',
                      title: ' Added '+ deviceType + ' with ID '+ deviceId
                })
              }
              else{
                Toast.fire({
                      icon: 'error',
                      title: ' Something went wrong'
                })
              }
            }
        });
  }
  function deleteDevice(id){
    if (confirm("Are you sure you want to delete device id: " + id)) {
        
        $.ajax({
            type: "POST",
            url: "updateDevice.php",
            data: ({
                delete: 1,
                deviceId: id
            }),
            cache: false,
            success: function(data) {
              location.reload();
            }
        });
    } else {
        return false;
    }
  }
  function switchChange(id, deviceType) {
      var Image_Id = document.getElementById(id);
      if (Image_Id.src.match("images/switch-on.png")) {
          Image_Id.src = "images/switch-off.png";
          $.ajax({
            type: "POST",
            url: "updateDevice.php",
            data: ({
                update: 1,
                value: 0,
                deviceId: id,
                deviceType: deviceType
            }),
            cache: false,
            success: function(data) {
              // console.log(data);
            }
          });
      }
      else {
          Image_Id.src = "images/switch-on.png";
          $.ajax({
            type: "POST",
            url: "updateDevice.php",
            data: ({
                update: 1,
                value: 1,
                deviceId: id,
                deviceType: deviceType
            }),
            cache: false,
            success: function(data) {
              // console.log(data);
            }
          });
      }
    }        

    function startOTA(){
      var Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000
      });
      var deviceId = document.getElementById('modal_deviceId').value;
      var deviceType = document.getElementById('modal_deviceType').value;
      var filename = document.getElementById('exampleInputFile').files[0].name;
      var time = document.getElementById('reservationtime').value;
      var new_version = document.getElementById('modal_newVersion').value;

      console.log(new_version);
      $.ajax({
            type : 'POST',
            url : 'updateDevice.php',
            data: ({
               ota: true,
               filepath: filename,
               deviceId: deviceId,
               deviceType: deviceType,
               scheduledDate: time,
               new_version: new_version
            }),
            success : function(response){
                // console.log(response);
                var data = JSON.parse(response);
                console.log(data['status']);
                
                if(data['status'] == 'true'){
                  Toast.fire({
                        icon: 'success',
                        title: ' OTA scheduled for device ' + deviceId
                  })
                }
                else{
                  Toast.fire({
                        icon: 'error',
                        title: ' Something went wrong'
                  })
                }
            }
      });
    }

    function loadModal(deviceId){
        $.ajax({
            type : 'POST',
            url : 'updateDevice.php',
            data: ({
               getDetails: true,
               deviceId: deviceId 
            }),
            success : function(response){
              var data = JSON.parse(response);
              console.log(data[0]['deviceId']);
              document.getElementById('modal_deviceId').value = data[0]['deviceId'];
              document.getElementById('modal_deviceType').value = data[0]['deviceType'];
              
              if(data[0]['deviceType']=="SWITCH"){
                document.getElementById('modal_eventType').value = "N.A.";
              }
              else{
                document.getElementById('modal_eventType').value = data[0]['eventType'];
              }
              createOTATable(data[0]['deviceId']);
              $('#modal-lg').modal('show');
            }
        });
      }
    
    
    function createOTATable(deviceId){
      $.ajax({
            type : 'POST',
            url : 'updateDevice.php',
            data: ({
               getOtaDetails: true,
               deviceId: deviceId
            }),
            success : function(response){
                var data = JSON.parse(response);
                // console.log(data[0]);

                var tableBody = document.getElementById("OtaTableBody");
                i = 0;
                // console.log(data.length);
                if(data == null){
                  var x = document.getElementById("ota_table_div");
                  x.style.display = "none";
                  return;
                }
                else{
                  var x = document.getElementById("ota_table_div");
                  x.style.display = "block";
                  $("#OtaTableBody tr").remove();
                }
                while(i < data.length){
                  // Sr Number
                  var tr = document.createElement("TR");
                  var td = document.createElement("TD");
                  var cell = document.createTextNode(i+1);
                  td.appendChild(cell);
                  tr.appendChild(td);

                  // OTA Version
                  td = document.createElement("TD");
                  cell = document.createTextNode(data[i]['new_version']);
                  td.appendChild(cell);
                  tr.appendChild(td);

                  // File Name
                  td = document.createElement("TD");
                  cell = document.createTextNode(data[i]['filepath']);
                  td.appendChild(cell);
                  tr.appendChild(td);
                  
                  // Schedule Time
                  td = document.createElement("TD");
                  cell = document.createTextNode(data[i]['start_time']);
                  td.appendChild(cell);
                  tr.appendChild(td);

                  //Status
                  td = document.createElement("TD");
                  cell = document.createTextNode(data[i]['status']);
                  td.appendChild(cell);
                  tr.appendChild(td);

                  //Append TR to tableBody
                  tableBody.appendChild(tr);
                  i++;
                }
                
            }
        })
    }
    $(document).ready(setInterval(function(){
      
      $.ajax({
        url: 'updateDevice.php',
        type: 'POST',
        data: ({
            loadValues: 1
        }),
        cache: false,
        success:function(response){
            data = JSON.parse(response);
            var i=0;
            while( i < data.length ){
              var value = data[i].value;
              var ts = data[i].last_heard;
              var deviceId = data[i].deviceId;
              // Update Value field in table
              var valueId = deviceId + "_id";
              var Valuetd = document.getElementById(valueId);
              if(Valuetd != null){
                Valuetd.innerHTML= value;
              }
              
              //Update Timestamp at last heard value
              var tsId = deviceId + "_ts";
              var Tstd = document.getElementById(tsId);
              if(Tstd != null){
                Tstd.innerHTML=ts;
              }

              i++;
            }
        }
      });
    }),500);

</script>
</body>

</html>
