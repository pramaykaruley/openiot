<?php
//Update below configurations as per your database credentials

$servername = "127.0.0.1";
$username = "root";
$password = "<YourPassword>";
$db = 'openiot';

$conn = mysqli_connect($servername, $username, $password, $db);
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}

// Global definitions
$MQTT_Topic = "gateway/openIoT/";


?>
