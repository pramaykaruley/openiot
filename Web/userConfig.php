<?php
/******************************** 
This file handles registration, updatePassword and other user related
information.

Javascript is using this file, we are first checking the handler called by JS and 
performing operations as per POST data from request.
*********************************/
session_start();
session_reset();
include "db_config.php";

// First check which request we got from JS
if(isset($_POST['register'])){
    // Extract data from POST request
    $firstname = $_POST['firstname'];
    $lastname = $_POST['lastname'];
    $email = $_POST['email'];
    $password = $_POST['password'];
    $confirmPass = $_POST['confirmPass'];

    // Check if user email is already present in database and send error message
    $sql = "SELECT * FROM users WHERE email='$email'";
    $result = mysqli_query($conn, $sql);
    if(mysqli_num_rows($result)){
        $myobj = new \stdClass();
        $myobj->status = "false";
        $myobj->msg = "Email is already register with us";
        echo json_encode($myobj);
        return;
    }

    // If not, insert the information in users database and send success message
    $sql = "INSERT INTO users (email, firstname, lastname, gender, password, regType, picture) 
            VALUES ('$email', '$firstname', '$lastname', '$gender', '$password','registered', './images/user.jpg');";
    $result = mysqli_query($conn, $sql);
    
    // Check if result has success or failure value 
    if($result){
        // On success, show success msg on registration page
        $myobj = new \stdClass();
        $myobj->status = "true";
        $myobj->msg = "Registered Successfully!!";
        echo json_encode($myobj);
        return;
    }
    else{
        //something went wrong, try to register again,
        // this should not occure, if occured, check sql query and update accrodingly
        $myobj = new \stdClass();
        $myobj->status = "false";
        $myobj->msg = "Error Occured, Try again..";
        echo json_encode($myobj);
        return;
    }
}
// Login module
if(isset($_POST['login'])){
    // extract data from POST request
    $email = $_POST['email'];
    $password = $_POST['password'];

    // Fetch data from users table

    $sql = "SELECT * FROM users WHERE email='$email' && password='$password';";
    $result = mysqli_query($conn, $sql);

    // iterate over result value to store it in SESSION veriable for further use
    if($row = mysqli_fetch_assoc($result)){
        // print_r($row);
        $_SESSION['email'] = $row['email'];
        $_SESSION['first_name'] = $row['firstname'];
        $_SESSION['last_name'] = $row['lastname'];
        $_SESSION['picture'] = $row['picture'];
        $_SESSION['name'] = $row['firstname']. " " . $row['lastname'];
        $_SESSION['active'] = 'true';

        $myobj = new \stdClass();
        $myobj->status = "true";
        $myobj->msg = "";
        
        echo json_encode($myobj);
    }
    else{
        $myobj = new \stdClass();
        $myobj->status = "false";
        $myobj->msg = "Incorrect Email or Password";
        echo json_encode($myobj);
        return;
    }

}

if(isset($_POST['lockscreen'])){
    $password = $_POST['password'];
    $email = $_SESSION['email'];

    $sql = "SELECT * FROM users WHERE email='$email' && password='$password';";
    $result = mysqli_query($conn, $sql);
    if($row = mysqli_fetch_assoc($result)){
        //set user session to active
        $_SESSION['active'] = 'true';

        $myobj = new \stdClass();
        $myobj->status = "true";
        $myobj->msg = "";
        
        echo json_encode($myobj);
    }
    else{
        $myobj = new \stdClass();
        $myobj->status = "false";
        $myobj->msg = "Incorrect Email or Password";
        echo json_encode($myobj);
        return;
    }

}



?>