<?php
session_start();
include "db_config.php";
$deviceId = $_GET['deviceId'];
$deviceType = $_GET['deviceType'];
$eventType = $_GET['eventType'];

// If session is not active, these internal pages can not be accessible
if(!isset($_SESSION['active'])){
  header("Location: error404.php");
  die();
}

$_SESSION['2last_url'] = isset($_SESSION['last_url']) ? $_SESSION['last_url'] : null;
$_SESSION['last_url'] = $_SERVER['HTTP_REFERER'];

?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Device Details</title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
  <!-- IonIcons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/adminlte.min.css">
  <link rel="stylesheet" href="plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
  <link rel="stylesheet" href="plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
  <link rel="stylesheet" href="plugins/select2/css/select2.min.css">
  <link rel="stylesheet" href="plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
</head>

<body class="hold-transition sidebar-mini">
<div class="wrapper">
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>

    </ul>


  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="dashboard.php" class="brand-link">
      <img src="images/iot-logo.jpg" alt="IoT Logo" class="brand-image img-circle elevation-10">
      <span class="brand-text font-weight-light">OpenIoT</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <?php
        include "sidebar.php";
      ?>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item">
            <a href="dashboard.php" class="nav-link">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
                
              </p>
            </a>
          </li>
         
          <li class="nav-item ">
            <a href="details.php" class="nav-link active">
              <i class="nav-icon fas fa-chart-pie"></i>
              <p>
                Device Details
              </p>
            </a>

          </li>
          <li class="nav-item">
            <a href="rules.php" class="nav-link">
              <i class="nav-icon fas fa-edit"></i>
              <p>
                Automation Rules
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="contributors.php" class="nav-link">
              <i class="nav-icon fas fa-user-secret"></i>
              <p>
                Contributors
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-user-lock"></i>
              <p>
                User Profile
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="lockscreen.php" class="nav-link">
                  <i class="fas fa-lock nav-icon"></i>
                  <p>Lockscreen</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="profile.php" class="nav-link">
                  <i class="fas fa-user-alt nav-icon"></i>
                  <p>Profile</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="logout.php" class="nav-link">
                  <i class="fas fa-sign-out-alt nav-icon"></i>
                  <p>Logout</p>
                </a>
              </li>
            </ul>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Device ID: <?php echo $deviceId; ?></h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Details</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-12">
            <div class="card">
              <div class="card-header border-2">
                <div class="d-flex justify-content-between">
                  <h3 class="card-title">DETAILS FOR <?php echo $eventType." ".$deviceType;?></h3>
                </div>
              </div>
              <div class="card-body">
                <div class="col-md-4">
                <label> Device: </label>
                  <select class="form-control select2bs4" style="width: 100%;" name="deviceType" id="details_deviceType" onchange="getDetails(this)">
                    <option selected="selected">Device Id</option>
                    <?php 
                      $sql = "SELECT * FROM iot WHERE deviceId in ( 
                                SELECT deviceId from devices WHERE user_email='".$_SESSION['email']."');";
                      $result = mysqli_query($conn, $sql);
                      while($row = mysqli_fetch_assoc($result)){
                    ?>                    
                        <option value="<?php echo $row['deviceId'];?>">
                          <?php echo $row['deviceId'];?> (<?php echo $row['deviceType']; ?>)
                        </option>
                    <?php 
                      }
                    ?>
                  </select>
                </div> 
                <div>
                  &nbsp;
                </div>
                <?php if($eventType=="TEMPERATURE" || $eventType=="HUMIDITY"){ ?>
                <div class="position-relative mb-4">
                  <canvas id="line-chart" height="200"></canvas>
                </div>
                <?php } else { ?>
                
                <div class="chart">
                  <canvas id="barChart" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
                </div>
                <?php } ?>
              </div>
            </div>

          </div>
        </div>
        <!-- /.row -->
        <div class="card">
              <div class="card-header">
                <h3 class="card-title">DATA FOR <?php echo $eventType." ".$deviceType; ?></h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="example2" class="table table-bordered table-striped">
                <?php 
                    if($deviceType=="SENSOR")
                      $sql = "SELECT * FROM iot_history WHERE deviceId='".$deviceId."'&& eventType='".$eventType."';";
                    else
                      $sql = "SELECT * FROM iot_history WHERE deviceId='".$deviceId."';";

                    $result = mysqli_query($conn, $sql);
                    $i = 1;
                    if(mysqli_num_rows($result) == 0){
                      ?> <H2> No updates available for this device... </H2> 
                    <?php
                    } else {
                    ?>
                  <thead>
                  <tr>
                    <th> # </th>
                    <th>Timestamp</th>
                    <th>Value</th>
                  </tr>
                  </thead>
                  <tbody>
                  
                    <?php 
                      while($row = mysqli_fetch_assoc($result)){
                    ?>
                    <tr>
                    <td><?php echo $i; ?> </td>
                    <td><?php echo $row['timestamp'];?></td>
                    <td><?php echo $row['value']; ?></td>
                    </tr>
                    <?php 
                      $i++;
                      }
                    }
                    ?>
                  </tbody>
                  <tfoot>
                  <tr>
                    <th> # </th>
                    <th>Timestamp</th>
                    <th>Value</th>
                  </tr>
                  </tfoot>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
      </div>
      <!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <!-- /.control-sidebar -->

  <!-- Main Footer -->
  <?php
  include "footer.php";
  ?>
</div>

<script src="plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE -->
<script src="dist/js/adminlte.js"></script>

<!-- OPTIONAL SCRIPTS -->
<script src="plugins/chart.js/Chart.min.js"></script>
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="plugins/select2/js/select2.full.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>

<script>
  var deviceId = "<?php echo $deviceId; ?>";
  var deviceType = "<?php echo $deviceType; ?>";
  var eventType = "<?php echo $eventType; ?>";
  // 'use strict'
  if(deviceType=="SWITCH" || eventType=="MOTION"){
    
    $(document).ready(function(){
      var sensorData = [],
        sensorLables = []
    $.ajax({
          url: 'updateDevice.php',
          type: 'POST',
          data: ({
              getHistory: 1,
              deviceId: deviceId,
              deviceType: deviceType,
              eventType: eventType
          }),
          cache: false,
          success:function(response){
              // console.log(response);
              var i = 0;
              data = JSON.parse(response);
              while(data.length - i > 25){
                i += 1;
              }
              for(i; i < data.length; i+=1){
                sensorLables.push(data[i].timestamp);  
                sensorData.push(data[i].value);
              }
              // console.log(sensoLables);
              var areaChartData = {
                  labels  : sensorLables,
                  
                  datasets: [
                    {
                      label               : 'SWITCH ON/OFF',
                      backgroundColor     : 'rgba(60,141,188,0.9)',
                      borderColor         : 'rgba(60,141,188,0.8)',
                      pointRadius          : true,
                      pointColor          : '#3b8bba',
                      pointStrokeColor    : 'rgba(60,141,188,1)',
                      pointHighlightFill  : '#fff',
                      pointHighlightStroke: 'rgba(60,141,188,1)',
                      data                : sensorData
                    },
                    
                  ]
                }

                var barChartCanvas = $('#barChart').get(0).getContext('2d')
                var barChartData = $.extend(true, {}, areaChartData)

                var barChartOptions = {
                  responsive              : true,
                  maintainAspectRatio     : false,
                  datasetFill             : false
                }

                new Chart(barChartCanvas, {
                  type: 'bar',
                  data: barChartData,
                  options: barChartOptions
                })
              }
            })
          });
       }
  else{
    var ticksStyle = {
      fontColor: '#495057',
      fontStyle: 'bold'
    }

    var mode = 'index'
    var intersect = true
  
    var $visitorsChart = $('#line-chart')
    $(document).ready(setInterval(function(){
      var sensorData = [],
        sensorLables = []
    $.ajax({
          url: 'updateDevice.php',
          type: 'POST',
          data: ({
              getHistory: 1,
              deviceId: deviceId,
              deviceType: deviceType,
              eventType: eventType
          }),
          cache: false,
          success:function(response){
            data = JSON.parse(response);
            var i = 0;
            while(data.length - i > 25){
                i += 1;
            }
            for(i; i < data.length; i+=1){
              
              sensorLables.push(data[i].timestamp);  
              sensorData.push(data[i].value);
            }
            var visitorsChart = new Chart($visitorsChart, {
            data: {
              labels: sensorLables,
              datasets: [{
                type: 'line',
                data: sensorData,
                backgroundColor: 'transparent',
                borderColor: '#007bff',
                pointBorderColor: '#007bff',
                pointBackgroundColor: '#007bff',
                fill: false,
                pointHoverBackgroundColor: '#007bff',
                pointHoverBorderColor    : '#007bff'
              },
            ]
            },
            options: {
              maintainAspectRatio: false,
              tooltips: {
                mode: mode,
                intersect: intersect
              },
              animation: {
                duration: 0
              },
              hover: {
                mode: mode,
                intersect: intersect
              },
              legend: {
                display: false
              },
              scales: {
                yAxes: [{
                  // display: false,
                  gridLines: {
                    display: false,
                    lineWidth: '10px',
                    color: 'rgba(0, 0, 0, .2)',
                    zeroLineColor: 'transparent'
                  },
                  ticks: $.extend({
                    beginAtZero: false,
                    suggestedMin: -5,
                    suggestedMax: 20
                  }, ticksStyle)
                }],
                xAxes: [{
                  display: true,
                  gridLines: {
                    display: true
                  },
                  ticks: ticksStyle
                }]
              }
            }
          })
          }
        });
    // eslint-disable-next-line no-unused-vars

  }),300);
}

$(function () {
    $('.select2').select2()

    //Initialize Select2 Elements
    $('.select2bs4').select2({
      theme: 'bootstrap4'
    })
    $("#example2").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false,
      "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
  });

function getDetails(sel){
  var deviceId = sel.options[sel.selectedIndex].value;
  
  $.ajax({
          url: 'updateDevice.php',
          type: 'POST',
          data: ({
              redirectForDetails: 1,
              deviceId: deviceId
          }),
          cache: false,
          success:function(response){
            // alert(response);
            window.location.replace(response);
          }
        })
  
}
</script>
</body>
</html>
