CREATE DATABASE openiot;
use openiot;

CREATE TABLE users (
id INT(6) UNSIGNED,
email VARCHAR(50) PRIMARY KEY,
firstname VARCHAR(30) NOT NULL,
lastname VARCHAR(30),
gender VARCHAR(6),
password VARCHAR(20),
picture VARCHAR(200),
reg_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
regType VARCHAR(10)
);

CREATE TABLE devices (
deviceId varchar(15) PRIMARY KEY,
user_email varchar(30),
timestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);

CREATE TABLE iot (
deviceId varchar(50) PRIMARY KEY,
deviceType ENUM('SENSOR', 'SWITCH', 'RGB LED'),
eventType ENUM('TEMPERATURE', 'MOTION', 'HUMIDITY'),
value varchar(20),
timestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
last_heard TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE iot_history (
deviceId varchar(50),
deviceType ENUM('SENSOR', 'SWITCH','RGB LED'),
eventType ENUM('TEMPERATURE', 'MOTION', 'HUMIDITY'),
value varchar(20),
timestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

DELIMITER $$

CREATE TRIGGER after_iot_update
AFTER UPDATE
ON iot FOR EACH ROW
BEGIN
    INSERT into iot_history (deviceId, deviceType, eventType, value) VALUES (new.deviceId, new.deviceType, new.eventType, new.value);
END $$

DELIMITER ;

CREATE TABLE ota (
deviceId varchar(50),
curr_version varchar(20),
new_version varchar(20),
filepath varchar(50),
start_time varchar(50),
status varchar(10)
);
