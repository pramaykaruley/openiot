<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>AdminLTE 3 | Registration Page</title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/adminlte.min.css">
</head>
<body class="hold-transition register-page">
<div class="register-box">
  <div class="register-logo">
    <a href="index2.html"><b>Open</b>IoT</a>
  </div>

  <div class="card">
    <div class="card-body register-card-body">
      <p class="login-box-msg">Register a new account</p>

      
        <div class="input-group mb-3">
          <input type="text" class="form-control" placeholder="First name" id="firstname">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-user"></span>
            </div>
          </div>
          <div> &nbsp; </div>
          <input type="text" class="form-control" placeholder="Last name" id="lastname">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-user"></span>
            </div>
          </div>
        </div>
        <div class="input-group mb-3">
          <input type="email" class="form-control" placeholder="Email" id="email">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-envelope"></span>
            </div>
          </div>
        </div>
        <div class="input-group mb-3">
          <input type="password" class="form-control" placeholder="Password" id="password">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>
        <div class="input-group mb-3">
          <input type="password" class="form-control" placeholder="Retype password" id="confirm_password">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-8">
            <div class="icheck-primary">
              <input type="checkbox" id="agreeTerms" name="terms" value="agree" id="terms">
              <label for="agreeTerms">
               I agree to the <a href="#">terms</a>
              </label>
            </div>
          </div>
          <!-- /.col -->
          <div class="col-4">
            <button type="button" class="btn btn-primary btn-block" onclick="registerUser()" >Register</button>
          </div>
          <!-- /.col -->
          
        </div>
        <div class="input-group mb-3">
              <div id="errorMsg" ></div>
        </div>
      

      <div class="social-auth-links text-center">
        <p>- OR -</p>
        
        <a href="#" class="btn btn-block btn-danger">
          <i class="fab fa-google-plus mr-2"></i>
          Sign up using Google+
        </a>
      </div>

      <a href="index.php" class="text-center">I already have a account</a>
    </div>
    <!-- /.form-box -->
  </div><!-- /.card -->
</div>
<!-- /.register-box -->

<!-- jQuery -->
<script src="plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<script>
function registerUser(){
  var firstname = document.getElementById("firstname").value;
  var lastname = document.getElementById("lastname").value;
  var email = document.getElementById("email").value;
  var password = document.getElementById("password").value;
  var c_password = document.getElementById("confirm_password").value;
  if(firstname == ""){
    document.getElementById("errorMsg").innerHTML = "Enter firstname";
    return;
  }
  if(email == ""){
    document.getElementById("errorMsg").innerHTML = "Enter Email";
    return;
  }
  if(password == ""){
    document.getElementById("errorMsg").innerHTML = "Enter Password";
    return;
  }
  if(c_password == ""){
    document.getElementById("errorMsg").innerHTML = "Retype Password";
    return;
  }
  if(password != c_password){
    document.getElementById("errorMsg").innerHTML = "Passwords are not matching";
    return;
  }
  document.getElementById("errorMsg").innerHTML = "";
  $.ajax({
        url: 'userConfig.php',
        type: 'POST',
        data: ({
            register: 1,
            firstname: firstname,
            lastname: lastname,
            email: email,
            password: password,
            confirmPass: c_password
        }),
        cache: false,
        success:function(response){
            var data = JSON.parse(response);
            if(data.status == 'false'){
              document.getElementById("errorMsg").innerHTML = data.msg;
            }
            if(data.status == 'true'){
              document.getElementById("errorMsg").innerHTML = data.msg;
            }
        }
      });
}
</script>
</body>
</html>
