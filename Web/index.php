<?php
require 'vendor/autoload.php';
include ("oauth2_config.php");
include ("db_config.php");

if(isset($_REQUEST['logout'])){
  session_unset();
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Open IoT</title>
  
  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/adminlte.min.css">
</head>
<body class="hold-transition login-page">
<div class="login-box">
  <!-- /.login-logo -->
  <div class="card card-outline card-primary">
    <div class="card-header text-center">
      <a href="index2.html" class="h1"><b>OPEN</b>IoT</a>
    </div>
    <div class="card-body">
      <p class="login-box-msg">Sign in to start your session</p>

        <div class="input-group mb-3">
          <input type="email" class="form-control" placeholder="Email" id="email">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-envelope"></span>
            </div>
          </div>
        </div>
        <div class="input-group mb-3">
          <input type="password" class="form-control" placeholder="Password" id="password">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-8">
            <div class="icheck-primary">
              <input type="checkbox" id="remember">
              <label for="remember">
                Remember Me
              </label>
            </div>
          </div>
          <!-- /.col -->
          <div class="col-4">
            <button type="button" onclick="loginUser()" class="btn btn-primary btn-block">Sign In</button>
          </div>
          <div class="col-12">
            <div id="errorMsg"></div>
          </div>
          <!-- /.col -->
        </div>

      <div class="social-auth-links text-center mt-2 mb-3">
        <a href="<?php echo $google_client->createAuthUrl(); ?>" class="btn btn-block btn-danger">
          <i class="fab fa-google-plus mr-2"></i> Sign in using Google+
        </a>
      </div>
      <!-- /.social-auth-links -->

      <p class="mb-1">
        <a href="forgot-password.html">I forgot my password</a>
      </p>
      <p class="mb-0">
        <a href="register.php" class="text-center">Register a new membership</a>
      </p>
    </div>
    <!-- /.card-body -->
  </div>
  <!-- /.card -->
</div>
<!-- /.login-box -->

<!-- jQuery -->
<script src="plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>

<script>
function loginUser(){
  var email = document.getElementById("email").value;
  var password = document.getElementById("password").value;
  
  if(email == ""){
    document.getElementById("errorMsg").innerHTML = "Enter Email";
    return;
  }
  if(password == ""){
    document.getElementById("errorMsg").innerHTML = "Enter Password";
    return;
  }
  
  document.getElementById("errorMsg").innerHTML = "";
  $.ajax({
        url: 'userConfig.php',
        type: 'POST',
        data: ({
            login: 1,
            email: email,
            password: password
        }),
        cache: false,
        success:function(response){
            var data = JSON.parse(response);
            if(data.status == 'false'){
              document.getElementById("errorMsg").innerHTML = data.msg;
            }
            if(data.status == 'true'){
              location.replace("dashboard.php");
            }
        }
      });
}
</script>
</body>
</html>
